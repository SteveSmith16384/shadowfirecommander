package com.scs.libgdx;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.scs.ecs.IEntity;
import com.scs.shadowfirecommander.Settings;
import com.scs.shadowfirecommander.components.IHoverable;
import com.scs.shadowfirecommander.gui.menu.IMenuClickListener;
import com.scs.shadowfirecommander.models.MouseData;

public abstract class AbstractGameScreen extends ApplicationAdapter implements InputProcessor, IMenuClickListener {

	protected OrthographicCamera camera;
	protected Viewport viewport;
	private HashMap<String, Texture> textures = new HashMap<String, Texture>();
	protected SpriteBatch batch;

	protected volatile boolean key[] = new boolean[256];
	protected List<MouseData> mouseDataList = new LinkedList<MouseData>();
	private long prevTime;
	protected int mouseX, mouseY;

	private boolean toggleFullscreen = false, fullscreen = false;
	protected boolean paused;
	protected float tpfSecs;

	public AbstractGameScreen() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void create() {
		camera = new OrthographicCamera(Settings.LOGICAL_WIDTH_PIXELS, Settings.LOGICAL_HEIGHT_PIXELS);
		camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
		viewport = new StretchViewport(Settings.WINDOW_WIDTH_PIXELS, Settings.WINDOW_HEIGHT_PIXELS, camera);
		camera.update();

		batch = new SpriteBatch();
		Gdx.input.setInputProcessor(this);

		prevTime = System.currentTimeMillis();


	}


	@Override
	public void render() {
		long now = System.currentTimeMillis();
		double diff = now - prevTime;
		tpfSecs = (float)(diff / 1000);
		if (tpfSecs > .5f) {
			tpfSecs = .5f;
		} else if (tpfSecs < Settings.MIN_TPF) { // 60 fps
			//Settings.p("Too fast!");
			int sleepTime = (int)((Settings.MIN_TPF - tpfSecs) * 1000); 
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			tpfSecs = Settings.MIN_TPF;
		}

		prevTime = now;

	}


	protected void checkFullScreenToggle() {
		if (this.toggleFullscreen) {
			this.toggleFullscreen = false;
			if (fullscreen) {
				Gdx.graphics.setWindowedMode(Settings.WINDOW_WIDTH_PIXELS, Settings.WINDOW_HEIGHT_PIXELS);
				batch.getProjectionMatrix().setToOrtho2D(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
				Gdx.gl.glViewport(0, 0, Gdx.graphics.getBackBufferWidth(), Gdx.graphics.getBackBufferHeight());
				fullscreen = false;
			} else {
				DisplayMode m = null;
				for(DisplayMode mode: Gdx.graphics.getDisplayModes()) {
					if (m == null) {
						m = mode;
					} else {
						if (m.width < mode.width) {
							m = mode;
						}
					}
				}

				Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
				batch.getProjectionMatrix().setToOrtho2D(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
				Gdx.gl.glViewport(0, 0, Gdx.graphics.getBackBufferWidth(), Gdx.graphics.getBackBufferHeight());
				fullscreen = true;
			}
		}
	}


	@Override
	public void resize(int width, int height) {
		viewport.update(width, height);
		viewport.apply();

		camera.viewportWidth = Settings.LOGICAL_WIDTH_PIXELS;
		camera.viewportHeight = Settings.LOGICAL_HEIGHT_PIXELS;
		camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
		camera.update();

	}


	@Override
	public void pause() {
		//Settings.p("Game paused");
		if (Settings.RELEASE_MODE) {
			paused = true;
		}
	}


	@Override
	public void resume() {
		//Settings.p("Game resumed");
		paused = false;
	}


	public Texture getTexture(String filename) {
		if (textures.containsKey(filename)) {
			return textures.get(filename);
		}
		Texture t = new Texture(filename);
		this.textures.put(filename, t);
		return t;
	}


	public void playSound(String filename) {
		if (filename != null && filename.length() > 0) {
			Settings.p("Playing " + filename);
			Sound sound = Gdx.audio.newSound(Gdx.files.internal(filename));
			sound.play(1f);
		}
	}


	@Override
	public void dispose() {
		batch.dispose();
		for(Texture t : this.textures.values()) {
			t.dispose();
		}
	}


	@Override
	public boolean keyDown(int keycode) {
		//Settings.p("key pressed: " + keycode);
		key[keycode] = true;
		return true;
	}


	@Override
	public boolean keyUp(int keycode) {
		//Settings.p("key released: " + keycode);
		key[keycode] = false;

		keyUp2(keycode);

		if (keycode == Keys.F1) {// && Gdx.app.getType() == ApplicationType.WebGL) {
			if (Gdx.app.getType() == ApplicationType.WebGL) {
				if (!Gdx.graphics.isFullscreen()) {
					Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayModes()[0]);
				}
			} else {
				toggleFullscreen = true;
			}
		}

		return true;
	}


	public abstract void keyUp2(int keycode);

	@Override
	public boolean keyTyped(char character) {
		return false;
	}


	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}


	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		MouseData mouseData = new MouseData();
		mouseData.screenX = screenX;//(int)newpos.x;
		mouseData.screenY = Settings.WINDOW_HEIGHT_PIXELS - screenY;
		mouseData.pointer = pointer;
		mouseData.button = button;

		this.mouseDataList.add(mouseData);
		return true;
	}


	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}


}
