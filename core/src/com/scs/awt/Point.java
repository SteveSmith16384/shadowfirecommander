package com.scs.awt;

/**
 * Use this class instead of the standard Java class since GWT doesn't know about that one.
 * @author StephenCS
 *
 */
public class Point {
	
	public int x, y;

	public Point() {
		this(0, 0);
	}
	
	
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	
	public Point subtract(int x, int y) {
		return new Point(this.x - x, this.y - y);
	}
	
	
	public Point normalizeLocal() {
		float m = (float)Math.sqrt(x*x + y*y);
		this.x /= m;
		this.y /= m;
		
		return this;
	}
	
	
	public float getLength() {
		return (float)Math.sqrt(x*x + y*y);
	}


	public float getDistance(int x, int y) {
		Point dist = this.subtract(x, y);
		return (float)Math.sqrt(dist.x * dist.x + dist.y * dist.y);
	}
	

	public float getDistance(Point end) {
		return getDistance(end.x, end.y);
	}
	
}
