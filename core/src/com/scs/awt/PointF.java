package com.scs.awt;

public class PointF {
	
	public float x, y;

	public PointF() {
		this(0, 0);
	}
	
	
	public PointF(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	
	public PointF clone() {
		return new PointF(x, y);
	}
	

	public PointF subtract(float x, float y) {
		return new PointF(this.x - x, this.y - y);
	}
	
	
	public PointF subtract(PointF p) {
		return subtract(p.x, p.y);
	}
	
	
	public PointF multLocal(float z) {
		this.x *= z;
		this.y *= z;
		return this;
	}
	
	
	public PointF addLocal(PointF p) {
		this.x += p.x;
		this.y += p.y;
		return this;
	}
	
	
	public PointF normalizeLocal() {
		float m = (float)Math.sqrt(x*x + y*y);
		this.x /= m;
		this.y /= m;
		
		return this;
	}

	
	public float getDistance(float x, float y) {
		PointF dist = this.subtract(x, y);
		return (float)Math.sqrt(dist.x * dist.x + dist.y * dist.y);
	}
	

	public float getDistance(PointF end) {
		return getDistance(end.x, end.y);
	}


	public float getLength() {
		return (float)Math.sqrt(x*x + y*y);
	}


}
