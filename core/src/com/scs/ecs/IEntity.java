package com.scs.ecs;

public interface IEntity {

	boolean isMarkedForRemoval();
	
}
