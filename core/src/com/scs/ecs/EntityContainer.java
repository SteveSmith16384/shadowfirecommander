package com.scs.ecs;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import com.scs.shadowfirecommander.components.IDisposable;

public class EntityContainer {

	public List<IEntity> entities; // In "draw" order

	public EntityContainer() {
		entities = new LinkedList<IEntity>();

	}

	
	public ListIterator<IEntity> getIterator() {
		return this.entities.listIterator();
	}
	

	public IEntity getEntity(int id) {
		return this.entities.get(id);
	}
	
	
	public List<IEntity> getEntities() {
		return this.entities;
	}
	

	/**
	 * Add entity to end of the last, i.e. drawn last, clicked on first.  E.g. fizogs, icons
	 * @param e
	 */
	public void addEntityToEnd(IEntity e) {
		this.entities.add(e);
	}


	/**
	 * Add entity to start of the last, i.e. drawn first, clicked on last.  E.g. map
	 * @param e
	 */
	public void addEntityToStart(IEntity e) {
		this.entities.add(0, e);
	}


	public void removeEntity(int idx) {
		IEntity e = this.entities.remove(idx);
		if (e instanceof IDisposable) {
			IDisposable id = (IDisposable)e;
			id.dispose();
		}
	}


	public void removeAllEntities() {
		while (this.entities.size() > 0) {
			this.removeEntity(0);
		}
		this.entities.clear();
	}

}
