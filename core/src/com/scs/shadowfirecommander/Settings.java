package com.scs.shadowfirecommander;

public class Settings {

	public static final String VERSION = "0.1";
	public static final boolean RELEASE_MODE = false;
	
	// Debug
	public static final boolean DEMO_MODE = !RELEASE_MODE && true;
	public static final boolean TEST_SHADER = !RELEASE_MODE && true;
	public static final boolean SHOW_SHOOT_LINES = !RELEASE_MODE && true;
	public static final boolean SHOW_ENEMIES = !RELEASE_MODE && true;
	public static final boolean SHOW_OUTLINE = !RELEASE_MODE && false;

	public static final int WINDOW_WIDTH_PIXELS = RELEASE_MODE ? 1024 : 512;
	public static final int WINDOW_HEIGHT_PIXELS = (int)(WINDOW_WIDTH_PIXELS * .68);
	public static final int LOGICAL_WIDTH_PIXELS = 512;
	public static final int LOGICAL_HEIGHT_PIXELS = (int)(LOGICAL_WIDTH_PIXELS * .68);

	public static float SQ_SIZE = LOGICAL_WIDTH_PIXELS / 20;
	
	public static final int SIDE_PLAYER = 0;
	public static final int SIDE_ENEMY = 1;
	public static final int SIDE_INDIFFERENT = -1; // e.g. Jones, Android, Newt

	// Settings
	public static final float BASIC_CREW_SPEED = 30f;
	public static final float SCROLL_SPEED = 150f;
	public static final float SHOT_INTERVAL = 1f;
	public static final float VIEW_DISTANCE = Settings.SQ_SIZE * 20;
	public static final float FLASH_TIME = 3f;
	public static final float MIN_TPF = .02f; //.016f;
	public static final long MISSION_TIME_MILLIS = 1000 * 60 * 100;
	public static final float TRACKER_DISTANCE = Settings.SQ_SIZE * 10;
	public static final float FIRE_EXT_DISTANCE = Settings.SQ_SIZE * 5;
	
	public static final String TITLE = RELEASE_MODE ? "Todo" : "SC";
	
	
	private Settings() {
	}

	
	public static final void p(String s) {
		System.out.println(s);
	}


	public static final void pe(String s) {
		System.err.println(s);
	}
	
}

