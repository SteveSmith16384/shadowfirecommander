package com.scs.shadowfirecommander.equipment;

import com.scs.ecs.IEntity;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.Settings;
import com.scs.shadowfirecommander.entities.Crewman;
import com.scs.shadowfirecommander.entities.Fire;
import com.scs.util.TimerF;

public class FireExtinguisher implements IEquipment, IProcessIfCarried {

	private TimerF timer;
	private MyGdxGame game;
	public Crewman carrier;

	public FireExtinguisher(MyGdxGame _game) {
		game = _game;

		timer = new TimerF(1000, false);
	}


	@Override
	public void processIfCarried(float tpfSecs) {
		if (this.timer.hasHit(tpfSecs)) {
			if (this.carrier == null) {
				Settings.pe(this + " has no carrier");
			} else {
				for(IEntity pe : game.entContainer.getEntities()) {
					if (pe instanceof Fire) {
						Fire fire = (Fire)pe;
						if (this.carrier.getDistanceTo(fire) <= Settings.FIRE_EXT_DISTANCE) {
							game.playSound("todo");
							//carrier.alertPlayer();
							return;
						}
					}
				}
			}
		}
	}

}
