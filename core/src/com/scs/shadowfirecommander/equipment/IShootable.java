package com.scs.shadowfirecommander.equipment;

public interface IShootable {

	float getShotPower();

	String getShootingSoundFile();
}
