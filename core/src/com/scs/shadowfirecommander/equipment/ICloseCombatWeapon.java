package com.scs.shadowfirecommander.equipment;

public interface ICloseCombatWeapon extends IEquipment {

	float getAttackPower();

	String getAttackSound();
	
}
