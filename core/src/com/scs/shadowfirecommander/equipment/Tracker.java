package com.scs.shadowfirecommander.equipment;

import com.scs.ecs.IEntity;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.Settings;
import com.scs.shadowfirecommander.entities.Crewman;
import com.scs.shadowfirecommander.entities.alien.Alien;
import com.scs.util.TimerF;

public class Tracker implements IEquipment, IProcessIfCarried {

	private TimerF timer;
	private MyGdxGame game;
	public Crewman carrier;

	public Tracker(MyGdxGame _game) {
		game = _game;

		timer = new TimerF(2000, false);
	}


	@Override
	public void processIfCarried(float tpfSecs) {
		if (this.timer.hasHit(tpfSecs)) {
			if (this.carrier == null) {
				Settings.pe(this + " has no carrier");
			} else {
				for(IEntity pe : game.entContainer.getEntities()) {
					if (pe instanceof Alien) {
						Alien unit = (Alien)pe;
						if (this.carrier.getDistanceTo(unit) <= Settings.TRACKER_DISTANCE) {
							game.playSound("tracker.wav");
							carrier.alertPlayer();
							return;
						}
					}
				}
			}
		}
	}

}
