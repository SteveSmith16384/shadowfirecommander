package com.scs.shadowfirecommander.equipment;

public interface IProcessIfCarried extends IEquipment {

	void processIfCarried(float tpfSecs);

}
