package com.scs.shadowfirecommander.equipment;

public class LaserRifle implements IEquipment, IShootable {

	public LaserRifle() {
	}

	@Override
	public float getShotPower() {
		return 80;
	}


	@Override
	public String getShootingSoundFile() {
		return "laser.wav"; 
	}



}
