package com.scs.shadowfirecommander.equipment;

public class PulseRifle implements IEquipment, IShootable {

	public PulseRifle() {
	}

	@Override
	public float getShotPower() {
		return 100;
	}


	@Override
	public String getShootingSoundFile() {
		return "burst fire.mp3";
	}



}
