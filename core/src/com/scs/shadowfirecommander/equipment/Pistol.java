package com.scs.shadowfirecommander.equipment;

public class Pistol implements IEquipment, IShootable {

	public Pistol() {
	}

	@Override
	public float getShotPower() {
		return 40;
	}


	@Override
	public String getShootingSoundFile() {
		return "laserpew.ogg"; 
	}



}
