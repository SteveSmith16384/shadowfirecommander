package com.scs.shadowfirecommander.modules;

import com.scs.awt.Point;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.Settings;
import com.scs.shadowfirecommander.entities.alien.Alien;
import com.scs.shadowfirecommander.entities.alien.JonesTheCat;
import com.scs.shadowfirecommander.map.MapData;
import com.scs.shadowfirecommander.map.MapLoader_SF;
import com.scs.shadowfirecommander.models.UnitStats;

public class AlienModule extends AbstractModule {

	public AlienModule(MyGdxGame game) {
		super(game);
	}


	@Override
	public void setup() {
		MapLoader_SF maploader = new MapLoader_SF(game);
		MapData mapdata = maploader.loadMapViaGdx("theassassins.csv");
		//mapdata.showMap();
		game.setMapData(mapdata);

		this.addCrew(0, new UnitStats("Bret", "bret.png", Settings.SIDE_PLAYER, 1, 100, 100), mapdata);
		this.addCrew(1, new UnitStats("Ash", "ash.png", Settings.SIDE_PLAYER, 1, 100, 100), mapdata);
		this.addCrew(2, new UnitStats("Kane", "kane.png", Settings.SIDE_PLAYER, 1.2f, 100, 75), mapdata);
		this.addCrew(3, new UnitStats("Lambert", "lambert.png", Settings.SIDE_PLAYER, .75f, 100, 150), mapdata);
		this.addCrew(4, new UnitStats("Dallas", "dallas.png", Settings.SIDE_PLAYER, 1, 100, 100), mapdata);
		this.addCrew(4, new UnitStats("Ripley", "ripley.png", Settings.SIDE_PLAYER, 1, 100, 100), mapdata);
		this.addCrew(5, new UnitStats("Parker", "parker.png", Settings.SIDE_PLAYER, 1, 100, 100), mapdata);

		{
			Point sq = this.getDeploySquare(mapdata, 1);
			JonesTheCat enemy = new JonesTheCat(game, Settings.SQ_SIZE * sq.x, Settings.SQ_SIZE * sq.y, new UnitStats("Jones", null, Settings.SIDE_ENEMY, 1, 100, 100));
			game.entContainer.addEntityToStart(enemy);
		}

		{
			Point sq = this.getDeploySquare(mapdata, 1);
			Alien enemy = new Alien(game, Settings.SQ_SIZE * sq.x, Settings.SQ_SIZE * sq.y, new UnitStats("Alien", null, Settings.SIDE_ENEMY, 1, 100, 100));
			game.entContainer.addEntityToStart(enemy);
		}
	}

}
