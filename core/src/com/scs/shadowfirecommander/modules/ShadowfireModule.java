package com.scs.shadowfirecommander.modules;

import com.scs.awt.Point;
import com.scs.lang.NumberFunctions;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.Settings;
import com.scs.shadowfirecommander.entities.Crewman;
import com.scs.shadowfirecommander.entities.EnemySoldier;
import com.scs.shadowfirecommander.gui.UnitFizog;
import com.scs.shadowfirecommander.map.MapData;
import com.scs.shadowfirecommander.map.MapLoader_SF;
import com.scs.shadowfirecommander.models.UnitStats;

public class ShadowfireModule extends AbstractModule {

	public ShadowfireModule(MyGdxGame game) {
		super(game);
	}


	public void setup() {
		MapLoader_SF maploader = new MapLoader_SF(game);
		MapData mapdata = maploader.loadMapViaGdx("theassassins.csv");
		//mapdata.showMap();
		game.setMapData(mapdata);

		this.addCrew(0, new UnitStats("Syylk", "syylk_head.png", Settings.SIDE_PLAYER, 1, 100, 100), mapdata);
		this.addCrew(1, new UnitStats("Sevrina", "sevrina_head.png", Settings.SIDE_PLAYER, 1, 100, 100), mapdata);
		this.addCrew(2, new UnitStats("Manto", "manto_head.png", Settings.SIDE_PLAYER, 1.2f, 100, 75), mapdata);
		this.addCrew(3, new UnitStats("Maul", "maul_real.png", Settings.SIDE_PLAYER, .75f, 100, 150), mapdata);
		this.addCrew(4, new UnitStats("Zark", "zark_head.png", Settings.SIDE_PLAYER, 1, 100, 100), mapdata);
		this.addCrew(5, new UnitStats("Torik", "torik_head.png", Settings.SIDE_PLAYER, 1, 100, 100), mapdata);

		for (int i=0 ; i<5 ; i++) {
			addEnemy(new UnitStats("Enemy_" + (i+1), "n/a", Settings.SIDE_ENEMY, 1, 50, 50), mapdata);
		}
	}


	private void addEnemy(UnitStats stats, MapData mapdata) {
		Point sq = this.getDeploySquare(mapdata, 1);
		EnemySoldier enemy = new EnemySoldier(game, Settings.SQ_SIZE * sq.x, Settings.SQ_SIZE * sq.y, stats);
		game.entContainer.addEntityToStart(enemy);
	}
	

}
