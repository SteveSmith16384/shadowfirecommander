package com.scs.shadowfirecommander.modules;

import com.scs.awt.Point;
import com.scs.lang.NumberFunctions;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.Settings;
import com.scs.shadowfirecommander.entities.Crewman;
import com.scs.shadowfirecommander.equipment.PulseRifle;
import com.scs.shadowfirecommander.gui.UnitFizog;
import com.scs.shadowfirecommander.map.MapData;
import com.scs.shadowfirecommander.models.UnitStats;

public abstract class AbstractModule {

	protected MyGdxGame game;
	
	public AbstractModule(MyGdxGame _game) {
		game = _game;
	}

	
	public abstract void setup();
	
	protected Crewman addCrew(int num, UnitStats stats, MapData mapdata) {
		Point sq = this.getDeploySquare(mapdata, 0);
		Crewman crewman = new Crewman(game, Settings.SQ_SIZE * sq.x, Settings.SQ_SIZE * sq.y, stats);
		game.entContainer.addEntityToStart(crewman);
		
		UnitFizog fizog = new UnitFizog(game, game.fontSmall, crewman, stats, Settings.WINDOW_WIDTH_PIXELS-UnitFizog.WIDTH, Settings.WINDOW_HEIGHT_PIXELS - UnitFizog.HEIGHT - (num * (UnitFizog.HEIGHT + 10)));
		game.entContainer.addEntityToEnd(fizog);
		
		return crewman;
	}


	protected Point getDeploySquare(MapData mapdata, int side) {
		return mapdata.deploy_squares[side].get(NumberFunctions.rnd(0, mapdata.deploy_squares[side].size()-1));
	}


}
