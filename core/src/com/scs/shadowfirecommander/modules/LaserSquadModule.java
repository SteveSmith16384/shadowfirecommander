package com.scs.shadowfirecommander.modules;

import com.scs.awt.Point;
import com.scs.lang.NumberFunctions;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.Settings;
import com.scs.shadowfirecommander.entities.Crewman;
import com.scs.shadowfirecommander.entities.EnemySoldier;
import com.scs.shadowfirecommander.entities.lasersquad.SternerRegnix;
import com.scs.shadowfirecommander.gui.UnitFizog;
import com.scs.shadowfirecommander.map.MapData;
import com.scs.shadowfirecommander.map.MapLoader_SF;
import com.scs.shadowfirecommander.models.UnitStats;

public class LaserSquadModule extends AbstractModule {

	public LaserSquadModule(MyGdxGame game) {
		super(game);
	}


	public void setup() {
		MapLoader_SF maploader = new MapLoader_SF(game);
		MapData mapdata = maploader.loadMapViaGdx("theassassins.csv");
		//mapdata.showMap();
		game.setMapData(mapdata);

		this.addCrew(0, new UnitStats("Cpl Jonlan", "syylk_head.png", Settings.SIDE_PLAYER, 1, 100, 100), mapdata);
		this.addCrew(1, new UnitStats("Pvt Anderson", "sevrina_head.png", Settings.SIDE_PLAYER, 1, 100, 100), mapdata);
		this.addCrew(2, new UnitStats("Pvt Stone", "manto_head.png", Settings.SIDE_PLAYER, 1.2f, 100, 75), mapdata);
		this.addCrew(3, new UnitStats("Pvt Harris", "maul_real.png", Settings.SIDE_PLAYER, .75f, 100, 150), mapdata);
		this.addCrew(4, new UnitStats("Pvt Turner", "zark_head.png", Settings.SIDE_PLAYER, 1, 100, 100), mapdata);

		for (int i=0 ; i<5 ; i++) {
			addEnemy(new UnitStats("Enemy_" + (i+1), "n/a", Settings.SIDE_ENEMY, 1, 50, 50), mapdata);
		}
		
		// Add Sterner
		Point sq = this.getDeploySquare(mapdata, 1);
		SternerRegnix enemy = new SternerRegnix(game, Settings.SQ_SIZE * sq.x, Settings.SQ_SIZE * sq.y, new UnitStats("Sterner Regnix", "n/a", Settings.SIDE_ENEMY, 1, 100, 100));
		game.entContainer.addEntityToStart(enemy);
	}

/*
	private void addCrew(int num, UnitStats stats, MapData mapdata) {
		Point sq = this.getDeploySquare(mapdata, 0);
		Crewman crewman = new Crewman(game, Settings.SQ_SIZE * sq.x, Settings.SQ_SIZE * sq.y, stats);
		game.addEntity(crewman);
		
		UnitFizog fizog = new UnitFizog(game, game.fontSmall, crewman, stats, Settings.LOGICAL_WIDTH_PIXELS-UnitFizog.WIDTH, Settings.LOGICAL_HEIGHT_PIXELS - UnitFizog.HEIGHT - (num * (UnitFizog.HEIGHT + 10)));
		game.addEntity(fizog);
	}
*/

	private void addEnemy(UnitStats stats, MapData mapdata) {
		Point sq = this.getDeploySquare(mapdata, 1);
		EnemySoldier enemy = new EnemySoldier(game, Settings.SQ_SIZE * sq.x, Settings.SQ_SIZE * sq.y, stats);
		game.entContainer.addEntityToStart(enemy);
	}
	
/*
	private Point getDeploySquare(MapData mapdata, int side) {
		return mapdata.deploy_squares[side].get(NumberFunctions.rnd(0, mapdata.deploy_squares[side].size()-1));
	}
*/
}
