package com.scs.shadowfirecommander.modules;

import com.scs.awt.Point;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.Settings;
import com.scs.shadowfirecommander.entities.Crewman;
import com.scs.shadowfirecommander.entities.alien.Alien;
import com.scs.shadowfirecommander.equipment.LaserRifle;
import com.scs.shadowfirecommander.equipment.Pistol;
import com.scs.shadowfirecommander.equipment.PulseRifle;
import com.scs.shadowfirecommander.map.MapData;
import com.scs.shadowfirecommander.map.MapLoader_SF;
import com.scs.shadowfirecommander.models.UnitStats;

public class AliensModule extends AbstractModule {

	public AliensModule(MyGdxGame game) {
		super(game);
	}


	@Override
	public void setup() {
		MapLoader_SF maploader = new MapLoader_SF(game);
		MapData mapdata = maploader.loadMapViaGdx("moonbaseassault.csv");
		//mapdata.showMap();
		game.setMapData(mapdata);

		Crewman c1 = this.addCrew(0, new UnitStats("Ripley", "bret.png", Settings.SIDE_PLAYER, 1, 100, 100), mapdata);
		c1.currentEquipment = new Pistol();
		Crewman c2 = this.addCrew(1, new UnitStats("Hicks", "ash.png", Settings.SIDE_PLAYER, 1, 100, 100), mapdata);
		c2.currentEquipment = new LaserRifle();
		//Crewman c3 = this.addCrew(2, new UnitStats("Burke", "kane.png", Settings.SIDE_PLAYER, 1.2f, 100, 75), mapdata);
		Crewman c4 = this.addCrew(3, new UnitStats("Bishop", "lambert.png", Settings.SIDE_PLAYER, .75f, 100, 150), mapdata);
		Crewman c5 = this.addCrew(4, new UnitStats("Hudson", "dallas.png", Settings.SIDE_PLAYER, 1, 100, 100), mapdata);
		c5.currentEquipment = new LaserRifle();
		Crewman c6 = this.addCrew(4, new UnitStats("Gorman", "ripley.png", Settings.SIDE_PLAYER, 1, 100, 100), mapdata);
		c6.currentEquipment = new Pistol();
		Crewman c7 = this.addCrew(5, new UnitStats("Vasquez", "parker.png", Settings.SIDE_PLAYER, 1, 100, 100), mapdata);
		c7.currentEquipment = new PulseRifle();
		Crewman c8 = this.addCrew(5, new UnitStats("Apone", "parker.png", Settings.SIDE_PLAYER, 1, 100, 100), mapdata);
		c8.currentEquipment = new LaserRifle();
		Crewman c9 = this.addCrew(5, new UnitStats("Drake", "parker.png", Settings.SIDE_PLAYER, 1, 100, 100), mapdata);
		c9.currentEquipment = new PulseRifle();

		for (int i=0 ; i<20 ; i++) {
			Point sq = this.getDeploySquare(mapdata, 1);
			Alien enemy = new Alien(game, Settings.SQ_SIZE * sq.x, Settings.SQ_SIZE * sq.y, new UnitStats("Alien", null, Settings.SIDE_ENEMY, 1, 100, 100));
			game.entContainer.addEntityToStart(enemy);
		}
	}
	
	
	/*@Override
	protected Crewman addCrew(int num, UnitStats stats, MapData mapdata) {
		Crewman crewman = super.addCrew(num, stats, mapdata);
		crewman.currentEquipment = new PulseRifle();
		return crewman;
	}*/

}
