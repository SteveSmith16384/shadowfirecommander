package com.scs.shadowfirecommander.gui;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.components.IDisposable;
import com.scs.shadowfirecommander.components.IDrawable;
import com.scs.shadowfirecommander.entities.AbstractEntity;

public class TextArea extends AbstractEntity implements IDrawable, IDisposable {

	private static final int MAX_LINES = 6;
	private static final float SPACING = 12;

	private List<String> lines = new LinkedList<String>();
	private BitmapFont font;

	public TextArea(MyGdxGame _game, BitmapFont _font) {
		super(_game, "GameLog");

		font = _font;
	}


	public void addLine(String s) {
		synchronized (lines) {
			this.lines.add(s);
			while (this.lines.size() > MAX_LINES) {
				this.lines.remove(0);
			}
		}
	}


	@Override
	public void draw(SpriteBatch batch) {
		font.setColor(Color.WHITE);

		float startPosY = MAX_LINES * SPACING;
		synchronized (lines) {
			for (int i=0 ; i<lines.size() ; i++) {
				String line = this.lines.get(i);
				this.font.draw(batch, line, 20, startPosY);
				startPosY -= SPACING;
			}
		}
	}


	@Override
	public void dispose() {
	}

}
