package com.scs.shadowfirecommander.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.scs.awt.RectF;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.Settings;
import com.scs.shadowfirecommander.components.IClickable;
import com.scs.shadowfirecommander.components.IDisposable;
import com.scs.shadowfirecommander.components.IDrawable;
import com.scs.shadowfirecommander.entities.AbstractEntity;
import com.scs.shadowfirecommander.entities.AbstractWalkingUnit;
import com.scs.shadowfirecommander.entities.Crewman;
import com.scs.shadowfirecommander.models.UnitStats;

public class UnitFizog extends AbstractEntity implements IDrawable, IClickable, IDisposable {

	public static final float WIDTH = Settings.LOGICAL_WIDTH_PIXELS / 10f;
	public static final float HEIGHT = Settings.LOGICAL_HEIGHT_PIXELS / 10f;
	
	private RectF screen_rect;
	protected Sprite sprite;
	protected UnitStats stats;
	protected Crewman entity;
	protected BitmapFont fontSmall;

	public UnitFizog(MyGdxGame game, BitmapFont _fontSmall, Crewman _entity, UnitStats _stats, float x, float y) {
		super(game, "UnitFizog_" + _stats.unit_name);
		
		fontSmall = _fontSmall;
		entity = _entity;
		stats = _stats;
		screen_rect = new RectF(x, y + HEIGHT, x+WIDTH, y);

		Texture img = game.getTexture(stats.fizogFilename);
		sprite = new Sprite(img);
		
		sprite.setPosition(x, y);
		sprite.setSize(WIDTH, HEIGHT);
	}

	
	@Override
	public RectF getScreenRect() {
		return screen_rect;
	}
	

	@Override
	public void draw(SpriteBatch batch) {
		if (this.stats.isAlive() == false) {
			batch.setColor(.5f, .5f, .5f, .5f);
		} else if (this.entity == game.getCurrentUnit()) {
			batch.setColor(1f, 1f, 1f, 1f);
		} else {
			batch.setColor(.9f, .9f, .9f, 1f);
		}
		sprite.draw(batch);
		
		fontSmall.setColor(Color.WHITE);
		this.fontSmall.draw(batch, this.stats.unit_name, sprite.getX(), sprite.getY()+HEIGHT);

		// Set status coloyur
		if (this.stats.current_health <= 0) {
			fontSmall.setColor(Color.GRAY);
		} else if (entity.enemy != null) {
			fontSmall.setColor(Color.RED);
		} else if (entity.destPixels != null) {
			fontSmall.setColor(Color.GREEN);
		} else {
			fontSmall.setColor(Color.WHITE);
		}
		this.fontSmall.draw(batch, this.entity.getStatusText(), sprite.getX(), sprite.getY());
		
		if (this.stats.current_health / this.stats.max_health < .25f) {
			fontSmall.setColor(Color.RED);
		} else {
			fontSmall.setColor(Color.WHITE);
		}
		this.fontSmall.draw(batch, "H:" + (int)this.stats.current_health, sprite.getX(), sprite.getY()+10);

	}


	@Override
	public void dispose() {
	}


	@Override
	public void clicked() {
		if (stats.isAlive()) {
			game.setCurrentUnit(entity);
		} else {
			game.appendToLog(stats.unit_name + " is dead");
		}
	}

}
