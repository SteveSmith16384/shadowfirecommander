package com.scs.shadowfirecommander.gui.menu;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.components.IClickable;
import com.scs.shadowfirecommander.components.IDebugDraw;
import com.scs.shadowfirecommander.entities.AbstractIcon;

public class MenuItem extends AbstractIcon implements IClickable {
	
	public static final float HEIGHT = 20;

	protected String text;
	private IMenuClickListener listener;
	
	public MenuItem(MyGdxGame game, String _text, String image, float x, float y, IMenuClickListener _listener) {
		super(game, "MenuItem", null, x, y, 100, HEIGHT);
		
		text = _text;
		listener = _listener;
	}
	

	@Override
	public void draw(SpriteBatch batch) {
		super.draw(batch);
		
		game.fontSmall.draw(batch, text, this.screenRect.left, this.screenRect.top-5);

	}
	
	
	@Override
	public void clicked() {
		listener.menuClicked(text);
		
	}

}
