package com.scs.shadowfirecommander.gui.menu;

public interface IMenuClickListener {

	void menuClicked(String text);
}
