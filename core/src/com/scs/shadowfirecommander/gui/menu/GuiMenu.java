package com.scs.shadowfirecommander.gui.menu;

import java.util.ArrayList;
import java.util.List;

import com.scs.shadowfirecommander.MyGdxGame;

public class GuiMenu {

	private MyGdxGame game;
	private float x, y;
	private List<MenuItem> menuItems = new ArrayList<MenuItem>();
	private IMenuClickListener listener;
	
	public GuiMenu(MyGdxGame _game, float _x, float _y, IMenuClickListener _listener) {
		game = _game;
		x =_x;
		y = _y;
		listener = _listener;
	}
	
	
	public void addMenuItem(String text) {
		MenuItem menuItem = new MenuItem(game, text, null, x, y-(MenuItem.HEIGHT * menuItems.size()), listener);
		this.menuItems.add(menuItem);
		
		game.entContainer.addEntityToEnd(menuItem);
	}
	
	
	public void clearMenuitems() {
		for (MenuItem menuItem : this.menuItems) {
			menuItem.markForRemoval();
		}
		this.menuItems.clear();
	}

}
