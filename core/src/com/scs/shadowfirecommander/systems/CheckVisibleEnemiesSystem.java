package com.scs.shadowfirecommander.systems;

import com.scs.ecs.IEntity;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.components.IOnlyDrawIfSeen;
import com.scs.shadowfirecommander.entities.Crewman;

public class CheckVisibleEnemiesSystem {

	private static final float CHECK_INTERVAL = 1f;

	private MyGdxGame game;
	private float nextCheck = 0;

	public CheckVisibleEnemiesSystem(MyGdxGame _game) {
		game = _game;
	}


	public void process(float tpfSecs) {
		nextCheck -= tpfSecs;
		if (nextCheck < 0) {
			nextCheck = CHECK_INTERVAL;
			for (int i=game.entContainer.getEntities().size()-1 ; i>=0 ; i--) {
				IEntity pe = game.entContainer.getEntity(i);
				if (pe instanceof IOnlyDrawIfSeen) {
					IOnlyDrawIfSeen ip = (IOnlyDrawIfSeen)pe;
					ip.setSeen(canBeSeen(ip));
				}
			}
		}

	}


	public boolean canBeSeen(IOnlyDrawIfSeen ip) {
		for (int i=game.entContainer.getEntities().size()-1 ; i>=0 ; i--) {
			IEntity pe = game.entContainer.getEntity(i);
			if (pe instanceof Crewman) {
				Crewman crew = (Crewman)pe;
				//crew.getDistanceTo(ip.getCentreX(), ip.getCentreY()) < Settings.VIEW_DISTANCE && 
				boolean result = crew.canSee(ip);// game.mapEntity.canSee(new PointF(crew.getCentreX(), crew.getCentreY()), new PointF(ip.getCentreX(), ip.getCentreY()));
				if (result) {
					return true;
				}
			}
		}
		return false;
	}

}
