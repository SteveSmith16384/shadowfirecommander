package com.scs.shadowfirecommander;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.scs.awt.PointF;
import com.scs.awt.RectF;
import com.scs.ecs.EntityContainer;
import com.scs.ecs.IEntity;
import com.scs.libgdx.AbstractGameScreen;
import com.scs.shadowfirecommander.components.IClickable;
import com.scs.shadowfirecommander.components.IDebugDraw;
import com.scs.shadowfirecommander.components.IDrawable;
import com.scs.shadowfirecommander.components.IHoverable;
import com.scs.shadowfirecommander.components.IProcessable;
import com.scs.shadowfirecommander.entities.AbstractWalkingUnit;
import com.scs.shadowfirecommander.entities.DebugSprite;
import com.scs.shadowfirecommander.entities.MapEntity;
import com.scs.shadowfirecommander.gui.TextArea;
import com.scs.shadowfirecommander.gui.menu.GuiMenu;
import com.scs.shadowfirecommander.map.AbstractMapSquare;
import com.scs.shadowfirecommander.map.MapData;
import com.scs.shadowfirecommander.map.MapDataTable;
import com.scs.shadowfirecommander.models.GameData;
import com.scs.shadowfirecommander.models.LineData;
import com.scs.shadowfirecommander.models.MouseData;
import com.scs.shadowfirecommander.modules.AbstractModule;
import com.scs.shadowfirecommander.modules.ShadowfireModule;
import com.scs.shadowfirecommander.systems.CheckVisibleEnemiesSystem;

/**
 * Origin is bottom left!
 *
 */
public class MyGdxGame extends AbstractGameScreen {

	public BitmapFont fontMed, fontSmall;
	private ShapeRenderer shapeRenderer;
	private Music music;
	public SoundEffects sfx = new SoundEffects();
	private List<LineData> linesToDraw = new LinkedList<LineData>();
	
	public EntityContainer entContainer = new EntityContainer();
	public MapEntity mapEntity;
	private MapData mapdata;
	public PointF viewOffset = new PointF(); // origin of map
	private AbstractWalkingUnit current_unit, tracked_unit;
	private DebugSprite debugSprite;
	private AbstractModule module;
	private GameData gameData;
	private String hoverText;
	private TextArea gameLog;
	private GuiMenu menu;

	// Time remaining
	private long endTimeMillis;

	private int gameStage = -1; // -1, -, or 1
	private boolean nextStage = false;

	// Flashing blips
	public float currentAlpha = 0f;
	private int alphaChange = 1;

	// Systems
	private CheckVisibleEnemiesSystem checkVisibleEnemies;

	Texture tex, tex2;

	ShaderProgram blurShader;
	FrameBuffer blurTargetA, blurTargetB;
	TextureRegion fboRegion;

	public static final int FBO_SIZE = 1024;

	public static final float MAX_BLUR = 2f;

	final String VERT =  
			"attribute vec4 "+ShaderProgram.POSITION_ATTRIBUTE+";\n" +
					"attribute vec4 "+ShaderProgram.COLOR_ATTRIBUTE+";\n" +
					"attribute vec2 "+ShaderProgram.TEXCOORD_ATTRIBUTE+"0;\n" +

			"uniform mat4 u_projTrans;\n" + 
			" \n" + 
			"varying vec4 vColor;\n" +
			"varying vec2 vTexCoord;\n" +

			"void main() {\n" +  
			"	vColor = "+ShaderProgram.COLOR_ATTRIBUTE+";\n" +
			"	vTexCoord = "+ShaderProgram.TEXCOORD_ATTRIBUTE+"0;\n" +
			"	gl_Position =  u_projTrans * " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" +
			"}";

	final String FRAG =
			"#ifdef GL_ES\n" + 
					"#define LOWP lowp\n" + 
					"precision mediump float;\n" + 
					"#else\n" + 
					"#define LOWP \n" + 
					"#endif\n" + 
					"varying LOWP vec4 vColor;\n" + 
					"varying vec2 vTexCoord;\n" + 
					"\n" + 
					"uniform sampler2D u_texture;\n" + 
					"uniform float resolution;\n" + 
					"uniform float radius;\n" + 
					"uniform vec2 dir;\n" + 
					"\n" + 
					"void main() {\n" + 
					"	vec4 sum = vec4(0.0);\n" + 
					"	vec2 tc = vTexCoord;\n" + 
					"	float blur = radius/resolution; \n" + 
					"    \n" + 
					"    float hstep = dir.x;\n" + 
					"    float vstep = dir.y;\n" + 
					"    \n" + 
					"	sum += texture2D(u_texture, vec2(tc.x - 4.0*blur*hstep, tc.y - 4.0*blur*vstep)) * 0.05;\n" + 
					"	sum += texture2D(u_texture, vec2(tc.x - 3.0*blur*hstep, tc.y - 3.0*blur*vstep)) * 0.09;\n" + 
					"	sum += texture2D(u_texture, vec2(tc.x - 2.0*blur*hstep, tc.y - 2.0*blur*vstep)) * 0.12;\n" + 
					"	sum += texture2D(u_texture, vec2(tc.x - 1.0*blur*hstep, tc.y - 1.0*blur*vstep)) * 0.15;\n" + 
					"	\n" + 
					"	sum += texture2D(u_texture, vec2(tc.x, tc.y)) * 0.16;\n" + 
					"	\n" + 
					"	sum += texture2D(u_texture, vec2(tc.x + 1.0*blur*hstep, tc.y + 1.0*blur*vstep)) * 0.15;\n" + 
					"	sum += texture2D(u_texture, vec2(tc.x + 2.0*blur*hstep, tc.y + 2.0*blur*vstep)) * 0.12;\n" + 
					"	sum += texture2D(u_texture, vec2(tc.x + 3.0*blur*hstep, tc.y + 3.0*blur*vstep)) * 0.09;\n" + 
					"	sum += texture2D(u_texture, vec2(tc.x + 4.0*blur*hstep, tc.y + 4.0*blur*vstep)) * 0.05;\n" + 
					"\n" + 
					"	gl_FragColor = vColor * vec4(sum.rgb, 1.0);\n" + 
					"}";

	@Override
	public void create() {
		super.create();
		
		tex = getTexture("greensquare.png");
		tex2 = getTexture("greensquare.png");
		tex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		tex2.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		//important since we aren't using some uniforms and attributes that SpriteBatch expects
		ShaderProgram.pedantic = false;

		blurShader = new ShaderProgram(VERT, FRAG);
		if (!blurShader.isCompiled()) {
			System.err.println(blurShader.getLog());
			System.exit(0);
		}
		if (blurShader.getLog().length()!=0)
			System.out.println(blurShader.getLog());

		//setup uniforms for our shader
		blurShader.begin();
		blurShader.setUniformf("dir", 0f, 0f);
		blurShader.setUniformf("resolution", FBO_SIZE);
		blurShader.setUniformf("radius", 1f);
		blurShader.end();

		blurTargetA = new FrameBuffer(Format.RGBA8888, FBO_SIZE, FBO_SIZE, false);
		blurTargetB = new FrameBuffer(Format.RGBA8888, FBO_SIZE, FBO_SIZE, false);
		fboRegion = new TextureRegion(blurTargetA.getColorBufferTexture());
		fboRegion.flip(false, true);


		shapeRenderer = new ShapeRenderer();

		// This doesn't work for HTML
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("SF Distant Galaxy.ttf"));
		FreeTypeFontParameter param = new FreeTypeFontParameter();
		param.size = 15;
		fontMed = generator.generateFont(param);
		//fontMed.setColor(Color.WHITE);

		param.size = 10;
		fontSmall = generator.generateFont(param);
		fontSmall.setColor(Color.WHITE);		

		generator.dispose();

		module = new ShadowfireModule(this); // AlienModule(this);// AlienModule(this);// ShadowfireModule(this);

		checkVisibleEnemies = new CheckVisibleEnemiesSystem(this) ;

		startGame();

		music = Gdx.audio.newMusic(Gdx.files.internal("gain_walkers_Sci_Fi_Atmosphere_03.mp3"));
		music.setLooping(true);
		music.setVolume(1f);
		music.play();

	}


	private void startGame() {
		entContainer.removeAllEntities();

		gameData = new GameData();

		module.setup();

		gameLog = new TextArea(this, fontSmall);
		entContainer.addEntityToEnd(this.gameLog);

		//debugSprite = new DebugSprite(this, 0, 0);
		//this.addEntityToEnd(debugSprite);

		menu = new GuiMenu(this, 10, Settings.LOGICAL_HEIGHT_PIXELS-30, this);
		//menu.addMenuItem("Hello!");
		//menu.addMenuItem("This is a menu");
		//menu.addMenuItem("Option 3");

		endTimeMillis = System.currentTimeMillis() + Settings.MISSION_TIME_MILLIS;

	}


	public void setMapData(MapData data) {
		this.mapdata = data;
		this.mapEntity = new MapEntity(this, data);
		entContainer.addEntityToStart(mapEntity);
	}


	void resizeBatch(int width, int height) {
		camera.setToOrtho(false, width, height);
		batch.setProjectionMatrix(camera.combined);
	}

	void renderEntities(SpriteBatch batch) {
		batch.draw(tex, 0, 0);
		batch.draw(tex2, tex.getWidth()+5, 30);
	}


	@Override
	public void render() {
		super.render();
		
		if (!paused) {
			gameData.millisElapsed += tpfSecs;
		}

		processInputs(tpfSecs);

		checkVisibleEnemies.process(tpfSecs);

		for (int i=entContainer.getEntities().size()-1 ; i>=0 ; i--) {
			IEntity pe = entContainer.getEntity(i);
			if (pe.isMarkedForRemoval()) {
				entContainer.removeEntity(i);
				continue;
			}
			if (pe instanceof IProcessable) {
				IProcessable ip = (IProcessable)pe;
				ip.process(tpfSecs);
			}
		}

		if (this.alphaChange > 0) {
			currentAlpha += tpfSecs * Settings.FLASH_TIME;
			if (this.currentAlpha >= 1f) {
				this.currentAlpha = 1f;
				this.alphaChange = -1;
			}
		} else {
			currentAlpha -= tpfSecs * Settings.FLASH_TIME;
			if (this.currentAlpha <= 0f) {
				this.currentAlpha = 0f;
				this.alphaChange = 1;
			}
		}

		if (this.tracked_unit != null) {
			this.scrollTo(this.tracked_unit.getWorldCentreX(), this.tracked_unit.getWorldCentreY());
		}

		// Start rendering ================================

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		//=================================================
		this.startBlur1();
		this.mapEntity.draw(batch);
		this.endBlur1();
		this.startBlur2();
		this.mapEntity.draw(batch);
		this.endBlur2();

		batch.begin();

		for(IEntity pe : this.entContainer.getEntities()) { // Draw in proper order
			if (pe instanceof IDrawable) {
				if (pe != this.mapEntity) {
					IDrawable id = (IDrawable)pe;
					id.draw(batch);
				}
			}
		}

		// Draw hover text
		if (hoverText != null && hoverText.length() > 0) {
			this.fontMed.draw(batch, this.hoverText, this.mouseX, this.mouseY);
		}
		String timeLeft = "Time: " + ((endTimeMillis - System.currentTimeMillis()) / 1000);
		this.fontMed.draw(batch, timeLeft, Settings.LOGICAL_WIDTH_PIXELS-50,  10);

		// Draw lines
		for (LineData lineData : this.linesToDraw) {
			shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
			shapeRenderer.setColor(Color.WHITE);
			shapeRenderer.line(lineData.x1,lineData.y1, lineData.x2, lineData.y2);
			shapeRenderer.end();

		}
		linesToDraw.clear();

		if (Settings.SHOW_OUTLINE) {
			for(IEntity pe : this.entContainer.getEntities()) {
				if (pe instanceof IDebugDraw) {
					IDebugDraw id = (IDebugDraw)pe;
					shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
					shapeRenderer.setColor(Color.YELLOW);
					RectF r = id.getScreenRect();
					shapeRenderer.rect(r.left, r.bottom, r.right - r.left, r.top-r.bottom);
					shapeRenderer.end();

				}
			}
		}

		batch.end();

		checkFullScreenToggle();

	}

	
	private void processInputs(float tpfSecs) {
		// Check inputs
		if (key[Input.Keys.LEFT] || key[Input.Keys.A]) {
			viewOffset.x -= Settings.SCROLL_SPEED * tpfSecs;
			tracked_unit = null;
		} else if (key[Input.Keys.RIGHT] || key[Input.Keys.D]) {
			viewOffset.x += Settings.SCROLL_SPEED * tpfSecs;
			tracked_unit = null;
		}
		if (key[Input.Keys.UP] || key[Input.Keys.W]) {
			viewOffset.y += Settings.SCROLL_SPEED * tpfSecs;
			tracked_unit = null;
		} else if (key[Input.Keys.DOWN] || key[Input.Keys.S]) {
			viewOffset.y -= Settings.SCROLL_SPEED * tpfSecs;
			tracked_unit = null;
		}

		while (mouseDataList.size() > 0) {
			MouseData mouseData = this.mouseDataList.remove(0);
			processMouseData(mouseData);
		}

	}

	private void startBlur1() {
		//Start rendering to an offscreen color buffer
		blurTargetA.begin();

		//Clear the offscreen buffer with an opaque background
		//Gdx.gl.glClearColor(0.5f, 0.5f, 0.5f, 1f);
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		//before rendering, ensure we are using the default shader
		batch.setShader(null);

		//resize the batch projection matrix before drawing with it
		resizeBatch(FBO_SIZE, FBO_SIZE);

		//now we can start drawing...
		batch.begin();
	}


	private void endBlur1() {
		//finish rendering to the offscreen buffer
		batch.flush();

		//finish rendering to the offscreen buffer
		blurTargetA.end();

		//now let's start blurring the offscreen image
		batch.setShader(blurShader);

		//since we never called batch.end(), we should still be drawing
		//which means are blurShader should now be in use

		//ensure the direction is along the X-axis only
		blurShader.setUniformf("dir", 1f, 0f);

		//update blur amount based on touch input
		//float mouseXAmt = Gdx.input.getX() / (float)Gdx.graphics.getWidth();
		blurShader.setUniformf("radius", MAX_BLUR);//mouseXAmt * MAX_BLUR);
	}
	
	
	private void startBlur2() {
		//our first blur pass goes to target B
		blurTargetB.begin();

		//we want to render FBO target A into target B
		fboRegion.setTexture(blurTargetA.getColorBufferTexture());

		//draw the scene to target B with a horizontal blur effect
		batch.draw(fboRegion, 0, 0);
	}
	
	
	private void endBlur2() {
		//flush the batch before ending the FBO
		batch.flush();

		//finish rendering target B
		blurTargetB.end();

		//now we can render to the screen using the vertical blur shader

		//update our projection matrix with the screen size
		resizeBatch(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		//update the blur only along Y-axis
		blurShader.setUniformf("dir", 0f, 1f);

		//update the Y-axis blur radius
		//float mouseYAmt = Gdx.input.getY() / (float)Gdx.graphics.getHeight();
		blurShader.setUniformf("radius", MAX_BLUR);

		//draw target B to the screen with a vertical blur effect 
		fboRegion.setTexture(blurTargetB.getColorBufferTexture());
		batch.draw(fboRegion, 0, 0);

		//reset to default shader without blurs 
		batch.setShader(null);

		//finally, end the batch since we have reached the end of the frame
		batch.end();
	}


	public void appendToLog(String s) {
		Settings.p(s);
		this.gameLog.addLine(s);
	}


	@Override
	public void dispose() {
		super.dispose();
		
		this.entContainer.removeAllEntities();

		shapeRenderer.dispose();
		if (fontMed != null) {
			fontMed.dispose();
		}
		if (fontSmall != null) {
			fontSmall.dispose();
		}
		this.sfx.dispose();
	}


	public void setCurrentUnit(AbstractWalkingUnit c) {
		this.current_unit = c;
		tracked_unit = c;
		if (c != null) {
			this.appendToLog(c.getUnitName() + " selected");
			this.scrollTo(c.getWorldCentreX(), c.getWorldCentreY());
		}
	}


	private void scrollTo(float px, float py) {
		// todo - scroll slowly
		this.viewOffset.x = px - (Settings.WINDOW_WIDTH_PIXELS/2);
		this.viewOffset.y = py - (Settings.WINDOW_HEIGHT_PIXELS/2);
	}


	public void addLineToDraw(LineData data) {
		linesToDraw.add(data);
	}


	public AbstractWalkingUnit getCurrentUnit() {
		return this.current_unit;
	}


	public void playFootstepSound() {
		//this.playSound("wooden-stairs-in-and-out/wooden-stairs-in-1.flac"); // todo - play other random footstep sounds
	}


	//#############################


	@Override
	public boolean scrolled(int amount) {
		return false;
	}


	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		mouseX = screenX;
		mouseY = Settings.WINDOW_HEIGHT_PIXELS - screenY;

		//Settings.p("Mouse at " + screenX + ", " + screenY);//(Settings.LOGICAL_HEIGHT_PIXELS - screenY));
		//debugSprite.setPosition(screenX, Settings.WINDOW_HEIGHT_PIXELS-screenY);
		//int worldX = screenX + (int)this.viewOffset.x;
		//int worldY = Settings.WINDOW_HEIGHT_PIXELS - screenY + (int)this.viewOffset.y;

		for(IEntity pe : this.entContainer.getEntities()) {
			if (pe instanceof IHoverable) {
				IHoverable ic = (IHoverable)pe;
				if (ic.getScreenRect().contains(mouseX, mouseY)) {
					hoverText = ic.getHoverText();
					//Settings.p("Hover text:" + this.hoverText);
					return false;
				}
			}
		}
		hoverText = "";
		return false;
	}


	private void processMouseData(MouseData mouseData) {
		int mapX = mouseData.screenX + (int)this.viewOffset.x;
		int mapY = mouseData.screenY + (int)this.viewOffset.y;

		// Clicked on entity?
		for (int i=entContainer.getEntities().size()-1 ; i>=0 ; i--) {
			IEntity pe = entContainer.getEntity(i);
			if (pe instanceof IClickable) {
				IClickable ic = (IClickable)pe;
				if (ic.getScreenRect().contains(mouseData.screenX, mouseData.screenY)) {
					ic.clicked();
					return;
				}
			}
		}

		// Click on map?
		if (this.current_unit != null) {
			AbstractMapSquare sq = this.mapdata.getSqByMapCoords(mapX/Settings.SQ_SIZE, mapY/Settings.SQ_SIZE);
			if (sq != null) {
				if (sq.major_type == MapDataTable.MT_FLOOR) {
					this.current_unit.setDest(mapX, mapY);
					this.appendToLog("Destination for " + current_unit.stats.unit_name + " selected");
				} else {
					this.appendToLog("That is not a valid square");
				}
			}
		}
	}


	@Override
	public void menuClicked(String text) {
		Settings.p("Menu " + text + " clicked");
		// todo
	}


	public void keyUp2(int keycode) {
		if (gameStage == -1) {
			nextStage = true;
		}
		if (keycode == 66) { //Enter
			if (gameStage == 1) {
				nextStage = true;
			}
		}
	}
	
	
}
