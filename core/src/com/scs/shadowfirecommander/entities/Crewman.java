package com.scs.shadowfirecommander.entities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.Speech;
import com.scs.shadowfirecommander.Speech.SpeechType;
import com.scs.shadowfirecommander.components.IClickable;
import com.scs.shadowfirecommander.components.IHoverable;
import com.scs.shadowfirecommander.models.UnitStats;

public class Crewman extends AbstractWalkingUnit implements IClickable, IHoverable {

	public Crewman(MyGdxGame game, float px, float py, UnitStats stats) {
		super(game, "Crewman_" + stats.unit_name, "greenblip.png", px, py, stats);

	}


	@Override
	public void process(float tpfSecs) {
		super.process(tpfSecs);
		
		if (enemy != null) {
			if (enemy.isAlive() == false || this.canSee(enemy) == false) {
				enemy = null;
			}
		} else {
			enemy = super.findClosestEnemy();
			if (enemy != null) {
				game.playSound(this.getSpeechFile(Speech.SpeechType.EnemySighted));
			}
		}
		
		if (enemy != null && this.canShoot()) {
			this.shoot(enemy);
		} else {
			super.moveToDestination(tpfSecs);
		}
		
		if (this.enemy == null && this.destPixels == null) {
			this.incHealth(tpfSecs * 10f);
		}
	}


	@Override
	public void draw(SpriteBatch batch) {
		sprite.setAlpha(game.currentAlpha);
		super.draw(batch);
	}


	@Override
	protected String getDeathSound() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getHoverText() {
		return this.stats.unit_name;
	}


	@Override
	public void clicked() {
		game.setCurrentUnit(this);
	}


	@Override
	protected String getSpeechFile(SpeechType type) {
		switch (type) {
		case RouteBlocked:
			// todo
			break;
		case Acknowledged:
			// todo
			break;
		case EnemySighted:
			// todo
			break;
		default:
			break;
		
		}
		return null;
	}


	public String getStatusText() {
		if (this.stats.current_health <= 0) {
			return "Dead";
		} else if (this.enemy != null) {
			return "See Enemy";
		} else if (this.destPixels != null) {
			return "Moving";
		} else {
			return "Idle";
		}
	}
	

	@Override
	public void killed(String reason) {
		super.killed(reason);
		if (game.getCurrentUnit() == this) {
			game.setCurrentUnit(null);
		}
	}
	

	public void alertPlayer() {
		// todo
	}
	
}
