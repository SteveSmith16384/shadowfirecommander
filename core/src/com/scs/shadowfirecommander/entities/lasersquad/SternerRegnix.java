package com.scs.shadowfirecommander.entities.lasersquad;

import java.util.List;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.Settings;
import com.scs.shadowfirecommander.Speech;
import com.scs.shadowfirecommander.Speech.SpeechType;
import com.scs.shadowfirecommander.components.IHoverable;
import com.scs.shadowfirecommander.components.IOnlyDrawIfSeen;
import com.scs.shadowfirecommander.entities.AbstractWalkingUnit;
import com.scs.shadowfirecommander.models.UnitStats;

public class SternerRegnix extends AbstractWalkingUnit implements IOnlyDrawIfSeen, IHoverable {

	private boolean visible;

	public SternerRegnix(MyGdxGame game, float px, float py, UnitStats stats) {
		super(game, stats.unit_name, "whiteblip.png", px, py, stats);
	}


	@Override
	public void process(float tpfSecs) {
		super.process(tpfSecs);

		if (enemy != null) {
			if (this.canSee(enemy) == false) {
				enemy = null;
			}
		} else {
			enemy = super.findClosestEnemy();	
			if (enemy != null) {
				game.playSound(this.getSpeechFile(Speech.SpeechType.EnemySighted));
				// call bodyguards!
				List<AbstractWalkingUnit> comrades = this.getComrades();
				for(AbstractWalkingUnit comrade : comrades) {
					if (comrade.enemy == null && comrade.destPixels == null) {
						comrade.setDest((int)this.getWorldCentreX(), (int)this.getWorldCentreY());
					}
				}
			}
		}

		if (enemy != null) {
			this.shoot(enemy);
		}
		// Can move AND shoot
		super.moveToDestination(tpfSecs);
	}


	@Override
	public void draw(SpriteBatch batch) {
		if (this.visible || Settings.SHOW_ENEMIES) {
			sprite.setAlpha(game.currentAlpha);
			super.draw(batch);
		}
	}


	@Override
	public void setSeen(boolean b) {
		this.visible = b;
	}


	@Override
	protected String getDeathSound() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getHoverText() {
		if (this.visible) {
			return stats.unit_name;
		} else {
			return "";
		}
	}


	@Override
	protected String getSpeechFile(SpeechType type) {
		return null;
	}


}
