package com.scs.shadowfirecommander.entities.aliens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.Settings;
import com.scs.shadowfirecommander.Speech.SpeechType;
import com.scs.shadowfirecommander.components.IHoverable;
import com.scs.shadowfirecommander.components.IOnlyDrawIfSeen;
import com.scs.shadowfirecommander.entities.AbstractWalkingUnit;
import com.scs.shadowfirecommander.entities.Crewman;
import com.scs.shadowfirecommander.models.UnitStats;

public class Newt extends AbstractWalkingUnit implements IOnlyDrawIfSeen, IHoverable {

	private boolean visible;
	private Crewman crewmanToFollow;
	
	public Newt(MyGdxGame game, float px, float py, UnitStats stats) {
		super(game, stats.unit_name, "whiteblip.png", px, py, stats);
	}


	@Override
	public void process(float tpfSecs) {
		super.process(tpfSecs);

		super.moveToDestination(tpfSecs);
	}


	@Override
	public void draw(SpriteBatch batch) {
		if (this.visible || Settings.SHOW_ENEMIES) {
			sprite.setAlpha(game.currentAlpha);
			super.draw(batch);
		}
	}


	@Override
	public void setSeen(boolean b) {
		this.visible = b;
	}


	@Override
	protected String getDeathSound() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getHoverText() {
		if (this.visible) {
			return "Newt";
		} else {
			return "";
		}
	}


	@Override
	protected String getSpeechFile(SpeechType type) {
		return null;
	}


}
