package com.scs.shadowfirecommander.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.scs.awt.RectF;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.components.IDebugDraw;
import com.scs.shadowfirecommander.components.IDisposable;
import com.scs.shadowfirecommander.components.IDrawable;

public class AbstractIcon extends AbstractEntity implements IDisposable, IDrawable, IDebugDraw {

	protected Sprite sprite;
	protected RectF screenRect = new RectF(); // For clicking
	
	public AbstractIcon(MyGdxGame game, String name, String image, float x, float y, float w, float h) {
		super(game, name);

		if (image != null && image.length() > 0) {
			Texture img = game.getTexture(image);
			sprite = new Sprite(img);
			sprite.setPosition(x, y);
			sprite.setSize(w, h);
		}
		
		screenRect.left = x;
		screenRect.bottom = y;
		screenRect.right = x + w;
		screenRect.top = y + h;

/*
		setWorldPosition(x, y);
	}

 	
 	protected void setWorldPosition(float x, float y) {
		this.worldPosX = x;
		this.worldPosY = y;*/
	}


	@Override
	public void draw(SpriteBatch batch) {
		if (sprite != null) {
			//sprite.setPosition(worldPosX - game.viewOffset.x, worldPosY - game.viewOffset.y);
			//this.updateScreenPos(worldPosX - game.viewOffset.x, worldPosY - game.viewOffset.y);
			sprite.draw(batch);
		}
	}


	@Override
	public RectF getScreenRect() {
		return this.screenRect;
	}

	
	public void dispose() {
	}

}
