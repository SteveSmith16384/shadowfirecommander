package com.scs.shadowfirecommander.entities.alien;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.scs.awt.PointF;
import com.scs.lang.NumberFunctions;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.Settings;
import com.scs.shadowfirecommander.Speech.SpeechType;
import com.scs.shadowfirecommander.components.IHoverable;
import com.scs.shadowfirecommander.components.IOnlyDrawIfSeen;
import com.scs.shadowfirecommander.entities.AbstractWalkingUnit;
import com.scs.shadowfirecommander.equipment.ICloseCombatWeapon;
import com.scs.shadowfirecommander.models.UnitStats;

public class Alien extends AbstractWalkingUnit implements IOnlyDrawIfSeen, IHoverable, ICloseCombatWeapon {

	private boolean visible;
	private boolean moveToNewDest = true;

	public Alien(MyGdxGame game, float px, float py, UnitStats stats) {
		super(game, "Alien", "redblip.png", px, py, stats);
	}


	@Override
	public void process(float tpfSecs) {
		super.process(tpfSecs);

		if (enemy != null) {
			if (enemy.isAlive() == false || this.canSee(enemy) == false) {
				enemy = null;
			}
		} else {
			enemy = super.findClosestEnemy();
			if (enemy != null) {
				PointF p = new PointF(enemy.getWorldCentreX(), enemy.getWorldCentreY()); // todo - don't do this every time
				this.setDest((int)p.x, (int)p.y);
				moveToNewDest = true;
				//todo game.playSound();
			}
		}

		if (enemy != null) {
			// Can see an enemy
			if (this.getDistanceTo(enemy) <= Settings.SQ_SIZE*2) {
				this.attack(this, enemy);
			}
		} //else {
		if (moveToNewDest && this.destPixels == null) {
			// One move 
			int x = NumberFunctions.rnd((int)(Settings.SQ_SIZE*1), (int)(game.mapEntity.mapdata.getMapWidth() * Settings.SQ_SIZE));
			int y = NumberFunctions.rnd((int)(Settings.SQ_SIZE*1), (int)(game.mapEntity.mapdata.getMapHeight() * Settings.SQ_SIZE));
			this.setDest(x, y);
			moveToNewDest = false;
		}
		if (!Settings.DEMO_MODE) {
			super.moveToDestination(tpfSecs);
		}
	}


	@Override
	public void draw(SpriteBatch batch) {
		if (this.visible || Settings.SHOW_ENEMIES) {
			sprite.setAlpha(game.currentAlpha);
			super.draw(batch);
		}
	}


	@Override
	public void setSeen(boolean b) {
		this.visible = b;
	}


	@Override
	protected String getDeathSound() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getHoverText() {
		if (this.visible) {
			return "Alien";
		} else {
			return "";
		}
	}


	@Override
	protected String getSpeechFile(SpeechType type) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getAttackSound() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public float getAttackPower() {
		return 100;
	}

}
