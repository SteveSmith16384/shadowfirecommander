package com.scs.shadowfirecommander.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.scs.awt.Point;
import com.scs.awt.PointF;
import com.scs.awt.RectF;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.Settings;
import com.scs.shadowfirecommander.components.IDebugDraw;
import com.scs.shadowfirecommander.components.IDisposable;
import com.scs.shadowfirecommander.components.IDrawable;

public class AbstractSimpleSprite extends AbstractEntity implements IDisposable, IDrawable, IDebugDraw {

	protected Sprite sprite;
	public float worldPosX, worldPosY, width, height;
	protected RectF screenRect = new RectF(); // For clicking
	
	public AbstractSimpleSprite(MyGdxGame game, String name, String image, float x, float y, float w, float h) {
		super(game, name);

		width = w;
		height = h;

		if (image != null && image.length() > 0) {
			Texture img = game.getTexture(image);
			sprite = new Sprite(img);
			sprite.setSize(w, h);
		}

		setWorldPosition(x, y);
	}

 	
 	protected void setWorldPosition(float x, float y) {
		this.worldPosX = x;
		this.worldPosY = y;
	}


	@Override
	public void draw(SpriteBatch batch) {
		if (sprite != null) {
			sprite.setPosition(worldPosX - game.viewOffset.x, worldPosY - game.viewOffset.y);
			this.updateScreenPos(worldPosX - game.viewOffset.x, worldPosY - game.viewOffset.y);
			sprite.draw(batch);
		}
	}


	public RectF getScreenRect() {
		return this.screenRect;
	}

	
	public float getDistanceTo(float cx, float cy) {
		return new PointF(this.getWorldCentreX()-cx, this.getWorldCentreY()-cy).getLength();
	}
	
	
	/**
	 * 
	 * @param x
	 * @param y
	 * @return Returns whether the move was successful
	 */
	public boolean moveBy(float x, float y) {
		if (x == 0 && y == 0) {
			return true;
		}

		worldPosX += x;
		worldPosY += y;

		return true;
	}


	public int getMapX() {
		return (int)(getWorldCentreX() / Settings.SQ_SIZE);
	}
	
	
	public int getMapY() {
		return (int)(getWorldCentreY() / Settings.SQ_SIZE);
	}
	
	
	public float getWorldCentreX() {
		return this.worldPosX+(this.width/2);
	}
	
	
	public float getWorldCentreY() {
		return this.worldPosY+(this.height/2);
	}
	
	
	protected void updateScreenPos(float screenPX, float screenPY) {
		screenRect.left = screenPX;
		screenRect.bottom = screenPY;
		screenRect.right = screenPX + width;
		screenRect.top = screenPY + height;
	}

	
	public void dispose() {
	}


	//@Override
	public Point getMapCoords() {
		return new Point((int)(this.worldPosX / Settings.SQ_SIZE), (int)(this.worldPosY / Settings.SQ_SIZE));
	}


}
