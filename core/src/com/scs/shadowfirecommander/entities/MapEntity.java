package com.scs.shadowfirecommander.entities;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.scs.awt.PointF;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.Settings;
import com.scs.shadowfirecommander.components.IDisposable;
import com.scs.shadowfirecommander.components.IDrawable;
import com.scs.shadowfirecommander.components.IProcessable;
import com.scs.shadowfirecommander.map.AbstractMapSquare;
import com.scs.shadowfirecommander.map.MapData;

public class MapEntity extends AbstractEntity implements IDrawable, IDisposable, IProcessable {	

	public MapData mapdata;
	private AbstractMapSquare[][] map;
	private int targetViewX, targetViewY;
	private boolean moveToTarget = false;

	public MapEntity(MyGdxGame game, MapData _mapdata) {
		super(game, "MapEntity");

		mapdata =_mapdata;
		map = mapdata.map;

		for (int y=0 ; y<map[0].length ; y++) {
			for (int x=0 ; x<map.length ; x++) {
				if (map[x][y] != null) {
					map[x][y].refreshData();
				}
			}
		}

	}


	public void setMapSquare(AbstractMapSquare sq, int mx, int my) {
		if (map[mx][my] != null) {
			map[mx][my].dispose();
		}
		map[mx][my] = sq;
	}


	public void draw(SpriteBatch batch) {
		int sx = (int)(game.viewOffset.x / Settings.SQ_SIZE);
		int ex = (int)((game.viewOffset.x + Settings.WINDOW_WIDTH_PIXELS) / Settings.SQ_SIZE);
		int sy = (int)(game.viewOffset.y / Settings.SQ_SIZE);
		int ey = (int)((game.viewOffset.y + Settings.WINDOW_HEIGHT_PIXELS) / Settings.SQ_SIZE);

		for (int y=sy ; y<ey+1 ; y++) {
			for (int x=sx ; x<ex+1 ; x++) {
				try {
					if (map[x][y] != null) {
						map[x][y].draw(batch);
					}
				} catch (ArrayIndexOutOfBoundsException e) {

				}
			}
		}
	}


	public boolean canSee(PointF start, PointF end) {
		PointF offset = end.subtract(start);
		offset.normalizeLocal().multLocal(Settings.SQ_SIZE/2);
		while (start.getDistance(end) > Settings.SQ_SIZE) {
			start.addLocal(offset);
			AbstractMapSquare sq = getMapSquare(start.x, start.y);
			if (sq.blocksView()) {
				return false;
			}
		}
		return true;
	}


	public List<AbstractMapSquare> getColliders(AbstractWalkingUnit unit, float posX, float posY, float w, float h) {
		int sx = (int)(posX / Settings.SQ_SIZE);
		int ex = (int)((posX+w) / Settings.SQ_SIZE);
		int sy = (int)(posY / Settings.SQ_SIZE);
		int ey = (int)((posY+h) / Settings.SQ_SIZE);

		List<AbstractMapSquare> blocks = new ArrayList<AbstractMapSquare>();
		for (int y=sy ; y<=ey ; y++) {
			for (int x=sx ; x<=ex ; x++) {
				if (x >= 0 && y >= 0 && x<map.length && y<map[x].length) {
					if (map[x][y] != null) {
						if (map[x][y].collides()) {
							blocks.add(map[x][y]); 
						} else {
							map[x][y].characterEntered(unit);
						}
					}
				}
			}			
		}

		return blocks;
	}


	public AbstractMapSquare getMapSquare(float px, float py) {
		int x = (int)(px / Settings.SQ_SIZE);
		int y = (int)(py / Settings.SQ_SIZE);

		if (x >= 0 && y >= 0 && x<map.length && y<map[x].length) {
			return map[x][y];	
		} else {
			return null;
		}
	}


	@Override
	public void dispose() {
		for (int y=0 ; y<map[0].length ; y++) {
			for (int x=0 ; x<map.length ; x++) {
				if (map[x][y] != null) {
					map[x][y].dispose();
				}
			}
		}
	}


	@Override
	public void process(float tpfSecs) {
		if (moveToTarget) {

		}

	}

}
