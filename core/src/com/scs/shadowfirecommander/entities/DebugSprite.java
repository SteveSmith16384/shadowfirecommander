package com.scs.shadowfirecommander.entities;

import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.Settings;

public class DebugSprite extends AbstractSimpleSprite {

	public DebugSprite(MyGdxGame game, float x, float y) {
		super(game, "DebugSprite", "bubble.png", x, y, Settings.SQ_SIZE, Settings.SQ_SIZE);
	}

}
