package com.scs.shadowfirecommander.entities;

import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.Settings;

public class Fire extends AbstractSimpleSprite {

	public Fire(MyGdxGame game, float px, float py) {
		super(game, "Fire", "orangeblip.png", px, py, Settings.SQ_SIZE, Settings.SQ_SIZE);
	}

}
