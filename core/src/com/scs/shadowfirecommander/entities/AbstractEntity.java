package com.scs.shadowfirecommander.entities;

import com.scs.ecs.IEntity;
import com.scs.shadowfirecommander.MyGdxGame;

public abstract class AbstractEntity implements IEntity {

	public int id;
	private static int nextId = 0;
	
	protected String entity_name;
	protected MyGdxGame game;
	protected boolean toBeRemoved;
	
	public AbstractEntity(MyGdxGame _game, String _entity_name) {
		game = _game;
		
		id = nextId++;
		entity_name =_entity_name;
	}

	
	public String getEntityName() {
		return this.entity_name;
		
	}
	
	
	public void markForRemoval() {
		toBeRemoved = true;	
	}
	
	
	@Override
	public boolean isMarkedForRemoval() {
		return toBeRemoved;
	}
	

	@Override
	public String toString() {
		return getEntityName();
	}
	

}
