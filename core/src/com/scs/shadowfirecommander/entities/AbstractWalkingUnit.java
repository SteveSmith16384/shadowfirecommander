package com.scs.shadowfirecommander.entities;

import java.util.ArrayList;
import java.util.List;

import com.scs.astar.AStar;
import com.scs.astar.Waypoints;
import com.scs.awt.Point;
import com.scs.awt.PointF;
import com.scs.ecs.IEntity;
import com.scs.lang.NumberFunctions;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.Settings;
import com.scs.shadowfirecommander.Speech;
import com.scs.shadowfirecommander.Speech.SpeechType;
import com.scs.shadowfirecommander.components.ICheckIfVisible;
import com.scs.shadowfirecommander.components.IProcessable;
import com.scs.shadowfirecommander.equipment.ICloseCombatWeapon;
import com.scs.shadowfirecommander.equipment.IEquipment;
import com.scs.shadowfirecommander.equipment.IProcessIfCarried;
import com.scs.shadowfirecommander.equipment.IShootable;
import com.scs.shadowfirecommander.map.AbstractMapSquare;
import com.scs.shadowfirecommander.models.LineData;
import com.scs.shadowfirecommander.models.UnitStats;

public abstract class AbstractWalkingUnit extends AbstractSimpleSprite implements IProcessable, ICheckIfVisible {

	public PointF destPixels;
	private Waypoints route;
	public UnitStats stats;
	public IEquipment currentEquipment;

	public AbstractWalkingUnit enemy;

	// Prevent shooting every frame
	private float timeUntilNextAttack;

	public AbstractWalkingUnit(MyGdxGame game, String name, String filename, float px, float py, UnitStats _stats) {
		super(game, name, filename, px, py, Settings.SQ_SIZE, Settings.SQ_SIZE);

		stats = _stats;
	}


	public void setDest(int px, int py) {
		//Settings.p("Destination selected for " + this.stats.unit_name);
		destPixels = new PointF(px, py);
		route = null;
		if (this == game.getCurrentUnit()) {
			game.playSound(this.getSpeechFile(Speech.SpeechType.Acknowledged));
		}
	}


	@Override
	public void process(float tpfSecs) {
		if (timeUntilNextAttack > 0) {
			timeUntilNextAttack -= tpfSecs;
		}
		if (this.currentEquipment != null && this.currentEquipment instanceof IProcessIfCarried) {
			IProcessIfCarried ipic = (IProcessIfCarried)this.currentEquipment;
			ipic.processIfCarried(tpfSecs);
		}
	}


	protected void moveToDestination(float tpfSecs) {
		if (destPixels != null) {
			float dist = destPixels.getDistance(this.getWorldCentreX(), this.getWorldCentreY());
			if (dist < 1) { // Stop them when reach destination
				endMovement();
				return;
			}			

			if (route == null) {
				PointF offset = this.destPixels.subtract(this.getWorldCentreX(), this.getWorldCentreY());
				offset.normalizeLocal();
				offset.multLocal(tpfSecs * stats.speed);
				move(offset);
			} else {
				Point nextMapPoint = route.getNextPoint();
				PointF pixelDest = new PointF(nextMapPoint.x * Settings.SQ_SIZE + (Settings.SQ_SIZE/2), nextMapPoint.y * Settings.SQ_SIZE + (Settings.SQ_SIZE/2));
				float d = pixelDest.getDistance(this.getWorldCentreX(), this.getWorldCentreY());
				if (d < 5) { // Must be close otherwise we'll start walking to the next square too soon and try to cut corners which means
					route.removeCurrentPoint();
					if (route.isEmpty()) {
						endMovement();
					}
				} else {
					PointF ourPos = new PointF(this.getWorldCentreX(), this.getWorldCentreY());
					PointF offset = pixelDest.subtract(ourPos);
					offset.normalizeLocal();
					offset.multLocal(tpfSecs * stats.speed);
					move(offset);
				}
			}
		} else { // No destination
			// todo move away from each other if on top
		}
	}


	private void move(PointF offset) {
		boolean success = false;
		if (this.route == null) {
			success = move(offset.x, offset.y);
		} else {
			success = move(offset.x, 0);
			success = move(0, offset.y) || success;
		}
		//Settings.p("Pos: " + this.posX + ", " + this.posY);
		if (success) {
			if (this == game.getCurrentUnit()) {
				game.playFootstepSound();
			}
		} else {
			if (this.route == null) {
				startAStar();
			} else {
				Settings.pe(this.entity_name + ": A* failed to move!");
				game.playSound(this.getSpeechFile(Speech.SpeechType.RouteBlocked));
				this.endMovement();
			}
		}
	}


	private boolean move(float offx, float offy) {
		if (offx == 0 && offy == 0) {
			return false;
		}
		PointF origPos = new PointF(this.worldPosX, this.worldPosY);
		this.worldPosX += offx;
		this.worldPosY += offy;
		this.setWorldPosition(worldPosX, worldPosY);

		float inset = Settings.SQ_SIZE * .1f; // Need this so we can fit through doors!
		List<AbstractMapSquare> blocks = game.mapEntity.getColliders(this, this.worldPosX+inset, this.worldPosY+inset, this.width-(inset*2), this.height-(inset*2));
		for (AbstractMapSquare block : blocks) {
			if (block.collides()) {
				this.setWorldPosition(origPos.x, origPos.y);
				return false;
			}
		}
		return true;
	}


	private void endMovement() {
		this.route = null;
		this.destPixels = null;
	}


	private void startAStar() {
		AStar a_star = new AStar(game.mapEntity.mapdata);
		a_star.findPath(this.getMapX(), this.getMapY(), (int)(this.destPixels.x/Settings.SQ_SIZE) , (int)(this.destPixels.y/Settings.SQ_SIZE), false);
		if (!a_star.wasSuccessful()) {
			Settings.p(this.entity_name + ": A* failed to find route");
			game.playSound(this.getSpeechFile(Speech.SpeechType.RouteBlocked));
			a_star = null;
		} else {
			//a_star.showRoute();
			route = a_star.getRoute();
		}
	}


	public boolean canSee(ICheckIfVisible unit) {
		boolean in_range = new PointF(this.getWorldCentreX()-unit.getWorldCentreX(), this.getWorldCentreY()-unit.getWorldCentreY()).getLength() <= Settings.VIEW_DISTANCE;
		if (in_range) {
			return game.mapEntity.canSee(new PointF(this.getWorldCentreX(), this.getWorldCentreY()), new PointF(unit.getWorldCentreX(), unit.getWorldCentreY()));
		}
		return false;
	}


	/*public AbstractWalkingUnit findVisibleEnemy() {
		for(IEntity pe : game.entities) {
			if (pe instanceof AbstractWalkingUnit) {
				AbstractWalkingUnit unit = (AbstractWalkingUnit)pe;
				if (unit.stats.side != this.stats.side && unit.isAlive() && unit.stats.side >= 0) {
					if (this.canSee(unit)) {
						return unit;
					}
				}
			}
		}
		return null;
	}*/


	public AbstractWalkingUnit findClosestEnemy() {
		AbstractWalkingUnit closest = null;
		float dist = 9999f;

		for(IEntity pe : game.entContainer.getEntities()) {
			if (pe instanceof AbstractWalkingUnit) {
				AbstractWalkingUnit unit = (AbstractWalkingUnit)pe;
				if (unit.stats.side != this.stats.side && unit.isAlive() && unit.stats.side >= 0) {
					float thisdist = this.getDistanceTo(unit);
					if (thisdist < dist) {
						if (this.canSee(unit)) {
							dist = thisdist;
							closest = unit;
						}
					}
				}
			}
		}
		return closest;
	}


	public float getDistanceTo(AbstractSimpleSprite unit) {
		return new PointF(this.getWorldCentreX()-unit.getWorldCentreX(), this.getWorldCentreY()-this.getWorldCentreY()).getLength();
	}


	public boolean canShoot() {
		return currentEquipment != null && this.currentEquipment instanceof IShootable;
	}

	public void shoot(AbstractWalkingUnit enemy) {
		if (this.canShoot()) {
			if (timeUntilNextAttack <= 0) {
				timeUntilNextAttack = Settings.SHOT_INTERVAL * NumberFunctions.rndFloat(0.8f, 1.2f); // Prevent everyone shooting at the same time

				//game.appendToLog(this.stats.unit_name + " is shooting!");

				if (Settings.SHOW_SHOOT_LINES) {
					LineData data = new LineData(this.getScreenRect().left, this.getScreenRect().top, enemy.getScreenRect().left, enemy.getScreenRect().top);
					game.addLineToDraw(data);
				}

				IShootable gun = (IShootable)currentEquipment;
				game.playSound(gun.getShootingSoundFile());
				float shot_power = gun.getShotPower() * NumberFunctions.rndFloat(.5f, 1.5f);
				float armour = enemy.stats.armour * NumberFunctions.rndFloat(.5f, 1.5f);
				if (shot_power > armour) {
					enemy.decHealth((int)(shot_power - armour), "shot by " + this.stats.unit_name);
				}
			}
		}
	}


	public void attack(ICloseCombatWeapon weapon, AbstractWalkingUnit enemy) {
		if (weapon != null) {
			if (timeUntilNextAttack <= 0) {
				timeUntilNextAttack = Settings.SHOT_INTERVAL * NumberFunctions.rndFloat(0.8f, 1.2f); // Prevent everyone shooting at the same time

				//game.appendToLog(this.stats.unit_name + " is shooting!");
				game.playSound(weapon.getAttackSound());

				float attack_power = weapon.getAttackPower() * NumberFunctions.rndFloat(.5f, 1.5f);
				float armour = enemy.stats.armour * NumberFunctions.rndFloat(.5f, 1.5f);
				if (attack_power > armour) {
					enemy.decHealth((int)(attack_power - armour), "shot by " + this.stats.unit_name);
				}
			}
		}
	}


	public void incHealth(float amt) {
		this.stats.current_health += amt;
		if (this.stats.current_health > this.stats.max_health) {
			this.stats.current_health = this.stats.max_health;
		}
	}


	public void decHealth(float amt, String reason) {
		game.appendToLog(stats.unit_name + " harmed " + amt + ": " + reason);
		this.stats.current_health -= amt;
		if (this.stats.current_health <= 0) {
			this.killed(reason);
		}
	}


	public void killed(String reason) {
		game.appendToLog(this.stats.unit_name + " killed: " + reason);
		this.markForRemoval();
		game.playSound(this.getDeathSound());

	}


	//protected abstract String getShootingSound();

	protected abstract String getDeathSound();

	//protected abstract String getAttackSound();

	protected abstract String getSpeechFile(SpeechType type);

	public String getUnitName() {
		return this.stats.unit_name;
	}


	public boolean isAlive() {
		return stats.isAlive();
	}


	protected List<AbstractWalkingUnit> getComrades() {
		ArrayList<AbstractWalkingUnit> list = new ArrayList<AbstractWalkingUnit>();

		for(IEntity pe : game.entContainer.getEntities()) {
			if (pe instanceof AbstractWalkingUnit) {
				AbstractWalkingUnit unit = (AbstractWalkingUnit)pe;
				if (unit.stats.side == this.stats.side && unit.isAlive() && unit != this) {
					list.add(unit);
				}
			}
		}

		return list;
	}
}