package com.scs.shadowfirecommander;

import java.util.HashMap;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.scs.shadowfirecommander.components.IDisposable;

public final class SoundEffects implements IDisposable {
	
	private HashMap<String, Sound> sfx = new HashMap<String, Sound>(); 

	public SoundEffects() {
	}

	
	public void play(String s) {
		if (!sfx.containsKey(s)) {
			sfx.put(s, Gdx.audio.newSound(Gdx.files.internal(s)));
		}
		Sound sound = sfx.get(s);
		sound.play();
	}
	
	
	@Override
	public void dispose() {
		Iterator<Sound> it = sfx.values().iterator();
		while (it.hasNext()) {
			Sound s = it.next();
			s.dispose();
		}
		sfx.clear();
	}

	
}
