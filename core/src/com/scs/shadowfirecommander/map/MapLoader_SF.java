package com.scs.shadowfirecommander.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.scs.awt.Point;
import com.scs.lang.NumberFunctions;
import com.scs.shadowfirecommander.MyGdxGame;

public class MapLoader_SF {

	private MyGdxGame game;
	
	public MapLoader_SF(MyGdxGame _game) {
		game = _game;
	}


	public MapData loadMapViaGdx(String filename) {
		//Gdx.app.log("MyTag", "Loading " + filename);

		FileHandle file = Gdx.files.internal(filename);
		String lines = file.readString();

		return this.Import(lines);
	}
	

	/* Commented out so that GWT works.
	public LevelData loadMapViaFile(String filename) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(filename));
		String lines = new String(encoded);
		return this.readData(lines);
	}
	 */


	private MapData Import(String lines) {
		MapData map = null;
		int size = 0;
		byte z=0;
		for(String line2 : lines.split("\\r?\\n")) {
			if (map == null) {
				size = NumberFunctions.ParseInt(line2.split(",")[0]);
				map = new MapData(game, size);
				map.random_deploy_squares.clear();
			} else {
				if (z<size) {
					for (byte x=0 ; x<size ; x++) { // Loop through each section of the line
						String[] line = line2.replaceAll("\"", "").split(",");
						ServerMapSquare sq = map.getSqByMapCoords(x, z);
						String data[] = null;
						try {
							data = line[x].split("\\|");
						} catch (java.lang.ArrayIndexOutOfBoundsException ex2) {
							ex2.printStackTrace();
						}
						for (int i=0 ; i<data.length ; i++) { // Loop through each bit of data in the cell
							if (data[i].length() > 0) {
								String subdata[] = data[i].split("\\:");
								if (subdata[0].equalsIgnoreCase("NOTHING")) {
									sq.major_type = MapDataTable.MT_NOTHING;
								} else if (subdata[0].equalsIgnoreCase("FLOOR")) {
									sq.major_type = MapDataTable.MT_FLOOR;
									sq.texture_code = Short.parseShort(subdata[1]);
								} else if (subdata[0].equalsIgnoreCase("WALL")) {
									sq.major_type = MapDataTable.MT_WALL;
									sq.texture_code = Short.parseShort(subdata[1]);
								} else if (subdata[0].equalsIgnoreCase("COMP")) {
									sq.major_type = MapDataTable.MT_COMPUTER;
									sq.texture_code = Short.parseShort(subdata[1]);
								} else if (subdata[0].equalsIgnoreCase("DOOR")) {
									sq.door_type = Byte.parseByte(subdata[1]);
									sq.major_type = MapDataTable.MT_FLOOR; // Override the default just in case
								} else if (subdata[0].equalsIgnoreCase("OWNER")) {
									//sq.owner_side = Byte.parseByte(subdata[1]);
								} else if (subdata[0].equalsIgnoreCase("DEPLOY")) {
									byte side = Byte.parseByte(subdata[1]);
									sq.deploy_sq_side = side;
									map.deploy_squares[side-1].add(new Point(x, z));
								} else if (subdata[0].equalsIgnoreCase("ESCAPE")) {
									sq.escape_hatch_side = Byte.parseByte(subdata[1]);
								} else if (subdata[0].equalsIgnoreCase("RND_DEPLOY")) {
									map.random_deploy_squares.add(new Point(x, z));
								} else if (subdata[0].equalsIgnoreCase("BARREL")) {
									// Do nothing at the mo
								} else if (subdata[0].equalsIgnoreCase("SCENERY")) {
									/*sq.scenery_code = Short.parseShort(subdata[1]);
									if (subdata.length >= 3) {
										sq.scenery_direction = Byte.parseByte(subdata[2]);
									}*/
								} else if (subdata[0].equalsIgnoreCase("RAISED_FLOOR")) {
									//sq.raised_texture_code = Short.parseShort(subdata[1]);
								} else {
									throw new RuntimeException("Unknown code: " + subdata[0]);
								}
							}
						}
					}
				}
				z++;
			}
		}

		return map;
	}


}

