package com.scs.shadowfirecommander.map;

import java.util.ArrayList;

import com.scs.astar.IAStarMapInterface;
import com.scs.awt.Point;
import com.scs.lang.NumberFunctions;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.Settings;

public class MapData implements IAStarMapInterface {

	public ServerMapSquare map[][];
	protected ArrayList<Point> doors = new ArrayList<Point>();
	public ArrayList<Point>[] deploy_squares = new ArrayList[2];
	public ArrayList<Point> random_deploy_squares = new ArrayList<Point>();
	private MyGdxGame game;

	public MapData(MyGdxGame _game, int MAX) {
		game = _game;
		map = new ServerMapSquare[MAX][MAX];

		deploy_squares[0] = new ArrayList<Point>();
		deploy_squares[1] = new ArrayList<Point>();

		this.clearMap();
	}


	// This is called by the client
	public MapData(ServerMapSquare[][] data) { 
		map = data;
	}


	protected void clearMap() {
		for (byte y=0 ; y<this.getMapHeight() ; y++) {
			for (byte x=0 ; x<this.getMapWidth() ; x++) {
				map[x][y] = new ServerMapSquare(game, MapDataTable.MT_NOTHING, x, y);//, (byte)0);
				//map[x][y].texture_code = TextureStateCache.TEX_CORRUGATED_WALL;
			}
		}
	}


	/*
	public ServerMapSquare getSq(byte x, byte z) {
		try {
			return map[x][z];
		} catch (java.lang.ArrayIndexOutOfBoundsException ex) {
			Settings.p("Error: sq " + x + "," + z + " not found.");
			ex.printStackTrace();
			return null;
		}
	}
	 */

	public ServerMapSquare getSqByMapCoords(float x, float z) {
		try {
			return map[(int)x][(int)z];
		} catch (java.lang.ArrayIndexOutOfBoundsException ex) {
			Settings.p("Error: sq " + x + "," + z + " not found.");
			ex.printStackTrace();
			return null;
		}
	}

	// =============================================================================

	protected void createRoomByCentre(byte centre_x, byte centre_y, byte l, byte d, short type, byte deploy_side) {
		this.createRoomByTopLeft((byte)(centre_x - (l/2)), (byte)(centre_y - (d/2)), l, d, type, deploy_side, false);
	}


	protected void createRoomByTopLeft(byte x, byte y, byte l, byte d, short type, byte deploy_side, boolean rnd_deploy_side) {
		for (byte y2=y ; y2<=y+d ; y2++) {
			for (byte x2=x ; x2<=x+l ; x2++) {
				AbstractMapSquare ms = map[x2][y2];
				ms.major_type = MapDataTable.MT_FLOOR;
				ms.texture_code = type;
				if (deploy_side >= 0) {
					ms.deploy_sq_side = deploy_side;
				} else if (rnd_deploy_side) {
					ms.deploy_sq_side = NumberFunctions.rndByte(1, deploy_side*-1);
				}
			}
		}

	}


	protected boolean isThereARoomAt(int x, int y, int l, int d) {
		for (int y2=y-1 ; y2<=y+d+1 ; y2++) {
			for (int x2=x-1 ; x2<=x+l+1 ; x2++) {
				if (map[x2][y2].major_type != MapDataTable.MT_NOTHING && map[x2][y2].major_type != MapDataTable.MT_WALL) {
					return true;
				}
			}
		}
		return false;
	}


	protected void addCorridorAndDoors(int sx, int sy, int ex, int ey, boolean extra_door) {
		int difx = NumberFunctions.sign(ex-sx);
		int dify = NumberFunctions.sign(ey-sy);

		// Across
		if (difx != 0) {
			boolean door_added = false;
			int x = sx;
			int y = sy;
			int len = Math.max(sx, ex) - Math.min(sx, ex);
			for (int c=0 ; c<len ; c++) {
				x += difx; 
				AbstractMapSquare ms = map[x][y]; 
				if (ms.major_type != MapDataTable.MT_FLOOR) {
					ms.major_type = MapDataTable.MT_FLOOR;
					//ms.texture_code = TextureStateCache.TEX_CORRIDOR1;
					if (!door_added || (extra_door && NumberFunctions.rnd(1, 10) == 1)) {
						if ((map[x][y+1].major_type == MapDataTable.MT_NOTHING && map[x][y-1].major_type == MapDataTable.MT_NOTHING) || (map[x][y+1].major_type == MapDataTable.MT_WALL && map[x][y-1].major_type == MapDataTable.MT_WALL)) {
							doors.add(new Point(x, y));
							door_added = true; 
						}
					}
				}
			}
		}
		// Down
		if (dify != 0) {
			boolean door_added = false;
			int x = ex;
			int y = sy;
			int len = Math.max(sy, ey) - Math.min(sy, ey);
			for (int c=0 ; c<len ; c++) {
				y += dify; 
				AbstractMapSquare ms = map[x][y];
				if (ms.major_type != MapDataTable.MT_FLOOR) {
					ms.major_type = MapDataTable.MT_FLOOR;
					//ms.texture_code = TextureStateCache.TEX_CORRIDOR1;
					if ((!door_added && y>sy) || (extra_door && NumberFunctions.rnd(1, 10) == 1)) {
						if ((map[x+1][y].major_type == MapDataTable.MT_NOTHING && map[x-1][y].major_type == MapDataTable.MT_NOTHING) || (map[x+1][y].major_type == MapDataTable.MT_WALL && map[x-1][y].major_type == MapDataTable.MT_WALL)) {
							doors.add(new Point(x, y));
							door_added = true; 
						}
					}
				}
			}
		}

	}


	public void showMap() {
		try {
			int w = map[0].length;
			int h = map.length;
			System.out.println("---------------------------------------------------");
			for(int z=0 ; z< h ; z++) {
				for(int x=0 ; x<w ; x++) {
					String s = " ";
					/*if (x == this.player_start_pos.x && z == this.player_start_pos.y) {
						s = "S";
					} else*/ if (map[x][z].door_type == MapDataTable.DOOR_NS) {
						s = "N";
					} else if (map[x][z].door_type == MapDataTable.DOOR_EW) {
						s = "E";
					} else if (map[x][z].major_type == MapDataTable.MT_FLOOR) {
						//s = "X";
					} else if (map[x][z].major_type == MapDataTable.MT_WALL) {
						s = "W";
					}
					System.out.print(s);
				}
				System.out.println("");
			}
			System.out.println("---------------------------------------------------");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	protected void addDoors() {
		for (int i=0 ; i<doors.size() ; i++) {
			Point p = doors.get(i);
			if (map[p.x+1][p.y].major_type == MapDataTable.MT_NOTHING && map[p.x-1][p.y].major_type == MapDataTable.MT_NOTHING) {
				map[p.x][p.y].door_type = MapDataTable.DOOR_EW;
			} else if (map[p.x][p.y+1].major_type == MapDataTable.MT_NOTHING && map[p.x][p.y-1].major_type == MapDataTable.MT_NOTHING) {
				map[p.x][p.y].door_type = MapDataTable.DOOR_NS;
			} else if (map[p.x+1][p.y].major_type == MapDataTable.MT_WALL && map[p.x-1][p.y].major_type == MapDataTable.MT_WALL) {
				map[p.x][p.y].door_type = MapDataTable.DOOR_EW;
			} else if (map[p.x][p.y+1].major_type == MapDataTable.MT_WALL && map[p.x][p.y-1].major_type == MapDataTable.MT_WALL) {
				map[p.x][p.y].door_type = MapDataTable.DOOR_NS;
			}
		}
	}


	//	A*

	public int getMapWidth() {
		return map.length;
	}

	public int getMapHeight() {
		return map[0].length;
	}

	public float getMapSquareDifficulty(int x, int z) {
		return 0;
	}

	public boolean isMapSquareTraversable(int x, int z) {
		AbstractMapSquare sq = this.getSqByMapCoords(x, z); 
		return sq.major_type == MapDataTable.MT_FLOOR;
	}

}
