package com.scs.shadowfirecommander.map;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.scs.shadowfirecommander.MyGdxGame;
import com.scs.shadowfirecommander.Settings;
import com.scs.shadowfirecommander.entities.AbstractWalkingUnit;

public class AbstractMapSquare {

	private MyGdxGame game;
	public int x, y;
	public int major_type;
	public short texture_code;
	public byte deploy_sq_side = -1;
	public byte escape_hatch_side = -1;
	public byte smoke_type;

	// Door data
	public byte door_type = -1; // See MapDataTable
	public long timeDoorOpened;

	private Texture img;
	private Sprite sprite;

	public AbstractMapSquare(MyGdxGame _game, int t, int _x, int _y) {//, int _destroyed) {//, boolean _door_open) {
		super();

		game = _game;
		x = _x;
		y = _y;
		this.major_type = t;
	}


	public void refreshData() {
		if (this.major_type == MapDataTable.MT_WALL) {
			img = game.getTexture("greensquare.png");
			sprite = new Sprite(img);
			sprite.setSize(Settings.SQ_SIZE, Settings.SQ_SIZE);
		} else if (this.door_type > 0) {
			img = game.getTexture("lightgreensquare.png");
			sprite = new Sprite(img);
			sprite.setSize(Settings.SQ_SIZE, Settings.SQ_SIZE);
		}
	}


	public boolean collides() {
		return this.major_type == MapDataTable.MT_WALL;
	}


	public void characterEntered(AbstractWalkingUnit crewman) {
		if (this.door_type > 0) {
			if (crewman.stats.side == Settings.SIDE_PLAYER) {
				if (System.currentTimeMillis() - timeDoorOpened > 7000) {
					// Play door sfx
					game.playSound("tspt_futuristic_door_slide_01_039.mp3");
				}
				timeDoorOpened = System.currentTimeMillis();
			}
		}
	}

	
	public boolean blocksView() {
		return this.major_type == MapDataTable.MT_WALL || this.door_type > 0;
	}


	public void draw(SpriteBatch batch) {
		if (sprite != null) {
			sprite.setPosition(x * Settings.SQ_SIZE - game.viewOffset.x, y * Settings.SQ_SIZE - game.viewOffset.y);
			sprite.draw(batch);
		}
	}


	public void dispose() {
		if (sprite != null) {
			//sprite.di
		}
	}

}

