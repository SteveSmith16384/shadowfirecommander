package com.scs.shadowfirecommander.map;

public class MapDataTable {

	// Major types
	public static final byte MT_NOTHING = 0;
	public static final byte MT_FLOOR = 1;
	public static final byte MT_COMPUTER = 2;
	public static final byte MT_WALL = 3;

	// Doors
	public static final byte DOOR_NS = 1;
	public static final byte DOOR_EW = 2;
	
	private MapDataTable() {
	}

}
