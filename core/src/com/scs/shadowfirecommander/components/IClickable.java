package com.scs.shadowfirecommander.components;

import com.scs.awt.RectF;

public interface IClickable {

	RectF getScreenRect();
	
	void clicked();
}
