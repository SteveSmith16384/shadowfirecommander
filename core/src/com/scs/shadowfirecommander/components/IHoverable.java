package com.scs.shadowfirecommander.components;

import com.scs.awt.RectF;

public interface IHoverable {

	RectF getScreenRect();

	String getHoverText();
	
}
