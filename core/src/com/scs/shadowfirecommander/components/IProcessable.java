package com.scs.shadowfirecommander.components;

public interface IProcessable {

	void process(float tpfSecs);
}
