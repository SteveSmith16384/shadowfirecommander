package com.scs.shadowfirecommander.components;

public interface IDisposable {

	void dispose();
	
}
