package com.scs.shadowfirecommander.components;

public interface ICheckIfVisible {

	public float getWorldCentreX();

	public float getWorldCentreY();
	

}
