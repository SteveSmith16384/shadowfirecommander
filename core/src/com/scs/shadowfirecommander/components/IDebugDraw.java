package com.scs.shadowfirecommander.components;

import com.scs.awt.RectF;

public interface IDebugDraw extends IDrawable {

	RectF getScreenRect(); // For debugging collisions

}
