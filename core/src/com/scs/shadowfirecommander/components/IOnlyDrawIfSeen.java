package com.scs.shadowfirecommander.components;

public interface IOnlyDrawIfSeen extends ICheckIfVisible {

	public void setSeen(boolean b);
	
}
