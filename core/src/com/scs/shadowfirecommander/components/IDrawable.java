package com.scs.shadowfirecommander.components;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface IDrawable {

	void draw(SpriteBatch batch);
	
}
