package com.scs.shadowfirecommander.models;

public class MouseData {

	public int screenX;
	public int screenY;
	public int pointer;
	public int button;
	
	public MouseData() {

	}

}
