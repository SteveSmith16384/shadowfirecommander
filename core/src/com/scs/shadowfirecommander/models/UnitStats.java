package com.scs.shadowfirecommander.models;

import com.scs.shadowfirecommander.Settings;

public class UnitStats {

	// Stats
	public String unit_name, fizogFilename;
	public int side;
	public float speed;
	public float max_health, current_health;
	//public float shot_power;
	public float armour;
	
	public UnitStats(String _name, String _fizogFilename, int _side, float _speedFrac, float _max_health, float _armour) {
		unit_name = _name;
		fizogFilename = _fizogFilename;
		side = _side;
		speed = Settings.BASIC_CREW_SPEED * _speedFrac;
		max_health = _max_health;
		this.current_health = _max_health;
		//shot_power = _shot_power;
		armour = _armour;
	}
	
	
	public boolean isAlive() {
		return this.current_health > 0;
	}
	
	
}
